import {
    gistsLoaded,
    gistsLoadFailed,
    gistLoaded,
    gistLoadFailed,
    saved,
    saveFailed,
    created,
    createFailed,
    deleted,
    deleteFailed
} from '../actions/GistActions';
import { authorizedGet, authorizedPatch, authorizedPost, authorizedDelete } from './requestHelper';

function getGists(token, url) {
    return authorizedGet(url, token)
        .then((response) => {
            gistsLoaded(response.data);
        })
        .catch((error) => {
            gistsLoadFailed(error.response.data);
        });
}

export function loadGists(token) {
    return getGists(token, 'https://api.github.com/gists');
}

export function loadStarredGists(token) {
    return getGists(token, 'https://api.github.com/gists/starred');
}

export function loadGist(token, gistId) {
    return authorizedGet(`https://api.github.com/gists/${gistId}`, token)
        .then((response) => {
            gistLoaded(response.data);
        })
        .catch((error) => {
            gistLoadFailed(error.response.data);
        });
}

export function saveGist(token, gistId, filename, content) {
    return authorizedPatch(`https://api.github.com/gists/${gistId}`, token, {
            files: {
                [filename]: content ? {
                    content
                } : null
            }
        })
        .then((response) => {
            saved(response.data.id);
        })
        .catch((error) => {
            saveFailed(error.response.data);
        });
}

export function createGist(token, filename, isPrivate) {
    return authorizedPost('https://api.github.com/gists', token, {
            description: filename,
            public: !isPrivate,
            files: {
                [filename]: {
                    content: 'Edit me!'
                }
            }
        })
        .then((response) => {
            created(response.data);
        })
        .catch((error) => {
            createFailed(error.response.data);
        });
}

export function deleteGist(token, gistId) {
    return authorizedDelete(`https://api.github.com/gists/${gistId}`, token)
        .then(() => {
            deleted(gistId);
        })
        .catch((error) => {
            deleteFailed(error.response.data);
        });
}
import { loaded, loadFailed } from '../actions/UserActions';
import { authorizedGet } from './requestHelper';

export function loadUser(token) {
    return authorizedGet('https://api.github.com/user', token)
        .then((response) => {
            loaded(response.data);
        })
        .catch((error) => {
            loadFailed(error.response.data);
        });
}
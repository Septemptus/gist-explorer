import axios from 'axios';
import { failed } from '../actions/AuthActions';

function authorizedCall(method, url, token, data) {
    return axios({
        method,
        url,
        data,
        headers: {
            Authorization: `token ${token}`
        }
    }).catch((error) => {
        if (error.response.status === 401) {
            failed(error.response.data);
        }

        throw error;
    });
}

export function authorizedGet(url, token) {
    return authorizedCall('get', `${url}?ts=${Date.now()}`, token);
}

export function authorizedPost(url, token, data) {
    return authorizedCall('post', url, token, data);
}

export function authorizedPatch(url, token, data) {
    return authorizedCall('patch', url, token, data);
}

export function authorizedDelete(url, token) {
    return authorizedCall('delete', url, token);
}
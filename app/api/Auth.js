import axios from 'axios';
import { authorized, failed } from '../actions/AuthActions';

export function authorize(code) {
    return axios.get(`/login/${code}?ts=${Date.now()}`)
        .then((response) => {
            authorized(response.data);
        })
        .catch(failed);
}

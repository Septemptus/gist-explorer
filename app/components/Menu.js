import React from 'react';
import AppBar from 'material-ui/AppBar';
import Drawer from 'material-ui/Drawer';
import MenuItem from 'material-ui/MenuItem';
import Divider from 'material-ui/Divider';
import { logOut } from '../actions/AuthActions';
import { loadGists, loadStarredGists } from '../actions/GistActions';

export default class Menu extends React.Component {
    constructor() {
        super();
        this.state = {
            open: false
        };
    }

    toggleMenu() {
        this.setState({
            open: !this.state.open
        });
    }

    loadGists() {
        loadGists();
        this.toggleMenu();
    }

    loadStarredGists() {
        loadStarredGists();
        this.toggleMenu();
    }

    logOut() {
        logOut();
        this.toggleMenu();
    }

    render() {
        return (
            <div>
                <AppBar onLeftIconButtonTouchTap={this.toggleMenu.bind(this)} title="Gist explorer"/>
                <Drawer docked={false} open={this.state.open} onRequestChange={this.toggleMenu.bind(this)}>
                    <MenuItem onTouchTap={this.loadGists.bind(this)}>My Gists</MenuItem>
                    <MenuItem onTouchTap={this.loadStarredGists.bind(this)}>Starred Gists</MenuItem>
                    <Divider/>
                    <MenuItem onTouchTap={this.logOut.bind(this)}>Log out</MenuItem>
                </Drawer>
            </div>
        );
    }
}

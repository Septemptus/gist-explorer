import React from 'react';
import Dialog from 'material-ui/Dialog';
import TextField from 'material-ui/TextField';
import FlatButton from 'material-ui/FlatButton';
import Checkbox from 'material-ui/Checkbox';

const KEY_CODE_ENTER = 13;
const width100Percent = {
    width: '100%'
};

export default class AddGistDialog extends React.Component {
    constructor() {
        super();
        this.state = {
            name: '',
            ticked: false
        };
    }

    onGistNameChange(e) {
        this.setState({
            name: e.target.value
        });
    }

    onKeyDown(e) {
        if (e.keyCode === KEY_CODE_ENTER) {
            this.create();
        }
    }

    onPrivateCheck(e, isChecked) {
        this.setState({
            ticked: isChecked
        });
    }

    create() {
        this.props.onCreate(this.state.name, this.state.ticked);
        this.props.onRequestClose();
        this.setState({
            name: '',
            ticked: false
        });
    }

    render() {
        const actions = [
            <FlatButton
                label="Cancel"
                secondary={true}
                onTouchTap={ this.props.onRequestClose }
                />,
            <FlatButton
                label="Submit"
                primary={true}
                onTouchTap={ this.create.bind(this) }
                />
        ];

        return (
            <Dialog
                actions={ actions }
                title={ this.props.title }
                modal={ false }
                open={ this.props.open }
                onRequestClose = { this.props.onRequestClose }>
                <TextField
                    style= { width100Percent }
                    value={ this.state.name }
                    onChange={ this.onGistNameChange.bind(this) }
                    onKeyDown={this.onKeyDown.bind(this)}
                    hintText={ this.props.message }
                    />
                {
                    this.props.withCheckbox ?
                        <Checkbox
                            onCheck={ this.onPrivateCheck.bind(this) }
                            label="Private?"
                        /> : null
                }

            </Dialog>
        );
    }
}
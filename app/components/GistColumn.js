import React from 'react';
import Paper from 'material-ui/Paper';
import GistList from './GistList';
import LabelFilter from './LabelFilter';

export default class GistColumn extends React.Component {
    render() {
        return (
            <Paper>
                <LabelFilter/>
                <GistList/>
            </Paper>
        );
    }
}

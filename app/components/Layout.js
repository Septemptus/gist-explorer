import React from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentAdd from 'material-ui/svg-icons/content/add';
import { Row, Col } from 'react-flexbox-grid/lib/index';
import { create } from '../actions/GistActions';
import { authorize, authorized } from '../actions/AuthActions';
import AuthStore from '../stores/AuthStore';
import UserStore from '../stores/UserStore';
import Menu from './Menu';
import AuthModal from './AuthModal';
import GistColumn from './GistColumn';
import GistPanel from './GistPanel';
import GistDialog from './GistDialog';
import SplashLoader from './SplashLoader';

const height100Percent = {
    height: '100%'
};

const buttonStyle = {
    position: 'fixed',
    right: '40px',
    bottom: '50px'
};

export default class Layout extends React.Component {
    constructor() {
        super();
        this.state = {
            loading: true,
            creatorOpen: false
        };

        this.onLoadChange = this.onLoadChange.bind(this);
    }

    componentDidMount() {
        if (AuthStore.isAuthorized()) {
            authorized(AuthStore.getToken());
        } else {
            authorize();
        }
    }

    componentWillMount() {
        AuthStore.on('change', this.onLoadChange);
        UserStore.on('change', this.onLoadChange);
    }

    componentWillUnmount() {
        AuthStore.removeListener('change', this.onLoadChange);
        UserStore.removeListener('change', this.onLoadChange);
    }

    onLoadChange() {
        this.setState({
            loading: AuthStore.isLoading() || UserStore.isLoading()
        });
    }

    openCreator() {
        this.setState({
            creatorOpen: true
        });
    }

    closeCreator() {
        this.setState({
            creatorOpen: false
        });
    }

    create(name, isPrivate) {
        create(name, isPrivate);
    }

    render() {
        return (
            <MuiThemeProvider style={ height100Percent }>
                <div>
                    <SplashLoader show={ this.state.loading }/>
                    <Menu/>
                    <AuthModal/>
                    <Row style={ height100Percent }>
                        <Col xs={12} md={3}>
                            <GistColumn/>
                        </Col>
                        <Col xs={12} md={9}>
                            <GistPanel/>
                        </Col>
                    </Row>
                    <FloatingActionButton
                        style={ buttonStyle }
                        onClick={ this.openCreator.bind(this) }
                    >
                        <ContentAdd />
                    </FloatingActionButton>
                    <GistDialog
                        title="Add Gist"
                        message="Enter a name for your new Gist"
                        withCheckbox={ true }
                        open={ this.state.creatorOpen }
                        onCreate={ this.create.bind(this) }
                        onRequestClose={ this.closeCreator.bind(this) }
                    />
                </div>
            </MuiThemeProvider>
        );
    }
}

import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import Popover from 'material-ui/Popover';
import Menu from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';
import FlashIcon from 'material-ui/svg-icons/image/flash-on';
import LabelStore from '../stores/LabelStore';
import FilterStore from '../stores/FilterStore';
import { setFilter } from '../actions/FilterActions';

export default class LabelFilter extends React.Component {
    constructor() {
        super();
        this.state = {
            open: false,
            anchor: null,
            labels: LabelStore.getLabels(),
            isOn: FilterStore.isOn(),
            activeFilters: FilterStore.getFilters()
        };

        this.onLabelsChange = this.onLabelsChange.bind(this);
        this.onFilterChange = this.onFilterChange.bind(this);
    }

    componentWillMount() {
        LabelStore.on('change', this.onLabelsChange);
        FilterStore.on('change', this.onFilterChange);
    }

    componentWillUnmount() {
        LabelStore.removeListener('change', this.onLabelsChange);
        FilterStore.removeListener('change', this.onFilterChange);
    }

    onLabelsChange() {
        this.setState({
            labels: LabelStore.getLabels()
        });
    }

    onFilterChange() {
        this.setState({
            isOn: FilterStore.isOn(),
            activeFilters: FilterStore.getFilters()
        });
    }

    handleClick(e) {
        this.setState({
            open: true,
            anchor: e.currentTarget
        });
    }

    handleClose() {
        this.setState({
            open: false,
            anchor: null
        });
    }

    render() {
        let filters = this.state.labels.map((label, idx) => {
            return (
                <MenuItem
                    key={idx}
                    insetChildren={true}
                    checked={this.state.activeFilters.includes(label)}
                    primaryText={label}
                    onClick={setFilter.bind(null, label)
                }/>
            );
        });

        if (filters.length === 0) {
            filters = <MenuItem insetChildren={true} primaryText="No filters available"/>;
        }

        return (
            <div>
                <RaisedButton
                    secondary={true}
                    fullWidth={true}
                    label="Filter"
                    labelPosition="before"
                    icon={ this.state.isOn ? <FlashIcon/> : null }
                    onTouchTap={this.handleClick.bind(this)}
                />
                <Popover
                    open={this.state.open}
                    anchorEl={this.state.anchor}
                    anchorOrigin={{horizontal: 'middle', vertical: 'bottom'}}
                    targetOrigin={{horizontal: 'left', vertical: 'top'}}
                    onRequestClose={this.handleClose.bind(this)}
                >
                    <Menu>
                        { filters }
                    </Menu>
                </Popover>
            </div>
        );
    }
}
import React from 'react';
import IconButton from 'material-ui/IconButton';
import Delete from 'material-ui/svg-icons/action/delete';
import { ListItem } from 'material-ui/List';
import { loadGist, deleteGist } from '../actions/GistActions';
import AreYouSure from './AreYouSure';

const focusStyle = {
    backgroundColor: 'rgba(0, 0, 0, 0.2)'
};

export default class Gist extends React.Component {
    constructor() {
        super();
        this.state = {
            confirmationOpen: false
        };
    }

    openConfirmation(e) {
        this.setState({
            confirmationOpen: true
        });
        e.stopPropagation();
    }

    closeConfirmation() {
        this.setState({
            confirmationOpen: false
        });
    }

    loadGist() {
        loadGist(this.props.gist.id);
    }

    deleteGist(e) {
        deleteGist(this.props.gist.id);
        this.closeConfirmation();
        e.stopPropagation();
    }

    render() {
        const iconButtonElement = (
            <IconButton
                touch={true}
                onClick={ this.openConfirmation.bind(this) }
            ><Delete/></IconButton>
        );

        return (
            <div>
                <ListItem
                    style={ this.props.active ? focusStyle : null }
                    onClick={this.loadGist.bind(this)}
                    primaryText={Object.keys(this.props.gist.files)[0]}
                    secondaryText={this.props.gist.public ? 'Public' : 'Private' }
                    rightIconButton={!this.props.readOnly ? iconButtonElement : null}
                />
                <AreYouSure
                    open={ this.state.confirmationOpen }
                    onYes={ this.deleteGist.bind(this) }
                    onNo={ this.closeConfirmation.bind(this) }
                />
            </div>
        );
    }
}

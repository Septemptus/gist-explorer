import React from 'react';
import { List, ListItem } from 'material-ui/List';
import CircularProgress from 'material-ui/CircularProgress';
import GistStore from '../stores/GistStore';
import StarredGistStore from '../stores/StarredGistStore';
import CurrentGistStore from '../stores/CurrentGistStore';
import Gist from './Gist';

const loaderStyle = {
    textAlign: 'center'
};

export default class GistList extends React.Component {
    constructor() {
        super();
        this.activeStore = GistStore;
        this.state = {
            currentGist: null,
            gists: [],
            error: false,
            loading: false,
            readOnly: false
        };

        this.onCurrentGistChange = this.onCurrentGistChange.bind(this);
        this.onGistsChange = this.onGistsChange.bind(this);
        this.onGistStoreOvertake = this.onGistStoreOvertake.bind(this);
        this.onStarredGistStoreOvertake = this.onStarredGistStoreOvertake.bind(this);
    }

    onCurrentGistChange() {
        this.setState({
            currentGist: CurrentGistStore.getCurrentGist()
        });
    }

    onGistsChange() {
        this.setState({
            gists: this.activeStore.getGists(),
            error: this.activeStore.hasError(),
            loading: this.activeStore.isLoading()
        });
    }

    onGistStoreOvertake() {
        this.activeStore = GistStore;
        this.setState({
            readOnly: false
        });
    }

    onStarredGistStoreOvertake() {
        this.activeStore = StarredGistStore;
        this.setState({
            readOnly: true
        });
    }

    componentWillMount() {
        GistStore.on('change', this.onGistsChange);
        StarredGistStore.on('change', this.onGistsChange);
        GistStore.on('overtake', this.onGistStoreOvertake);
        StarredGistStore.on('overtake', this.onStarredGistStoreOvertake);
        CurrentGistStore.on('change', this.onCurrentGistChange);
    }

    componentWillUnmount() {
        GistStore.removeListener('change', this.onGistsChange);
        StarredGistStore.removeListener('change', this.onGistsChange);
        GistStore.removeListener('overtake', this.onGistStoreOvertake);
        StarredGistStore.removeListener('overtake', this.onStarredGistStoreOvertake);
        CurrentGistStore.removeListener('change', this.onCurrentGistChange);
    }

    render() {
        const error = (
            <ListItem primaryText={'Could not load Gists'}></ListItem>
        );

        const loader = (
            <ListItem style={ loaderStyle }><CircularProgress/></ListItem>
        );

        let items = this.state.gists.map((gist, idx) => {
            return <Gist
                    readOnly={this.state.readOnly}
                    gist={gist}
                    key={idx}
                    active={ gist.id === (this.state.currentGist && this.state.currentGist.id) }
                />;
        });

        if (items.length === 0) {
            items = [
                <ListItem primaryText={'No Gists available'} key={0}/>
            ];
        }

        return (
            <List>
                {
                    this.state.error ?
                        error :
                        this.state.loading ?
                            loader :
                            items
                }
            </List>
        );
    }
}

import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import GistDialog from './GistDialog';
import AreYouSure from './AreYouSure';

const buttonStyle = {
    margin: 5
};

const buttonContainerStyle = {
    float: 'right',
    clear: 'right'
};

export default class Toolbar extends React.Component {
    constructor() {
        super();
        this.state = {
            confirmationOpen: false,
            creatorOpen: false
        };
    }

    closeCreator() {
        this.setState({
            creatorOpen: false
        });
    }

    openCreator() {
        this.setState({
            creatorOpen: true
        });
    }

    openConfirmation() {
        this.setState({
            confirmationOpen: true
        });
    }

    closeConfirmation() {
        this.setState({
            confirmationOpen: false
        });
    }

    render() {
        return (
            <div style={ buttonContainerStyle }>
                <RaisedButton
                    label="Delete file"
                    disabled={ this.props.readOnly }
                    style={ buttonStyle }
                    onTouchTap={ this.openConfirmation.bind(this) }/>
                <RaisedButton
                    label="Add file"
                    disabled={ this.props.readOnly }
                    style={ buttonStyle }
                    onTouchTap={ this.openCreator.bind(this) }/>
                <RaisedButton
                    label="Discard"
                    disabled={ this.props.readOnly }
                    secondary={true}
                    style={ buttonStyle }
                    onTouchTap={ this.props.onDiscard }/>
                <RaisedButton
                    label="Save"
                    disabled={ this.props.readOnly }
                    primary={true}
                    style={ buttonStyle }
                    onTouchTap={ this.props.onSave }/>
                <GistDialog
                    title="New file"
                    message="Enter a name for your new file"
                    open={ this.state.creatorOpen }
                    onCreate={ this.props.onAdd }
                    onRequestClose={ this.closeCreator.bind(this) }
                />
                <AreYouSure
                    open={ this.state.confirmationOpen }
                    onYes={ this.props.onDelete }
                    onNo={ this.closeConfirmation.bind(this) }
                />
            </div>
        );
    }
}
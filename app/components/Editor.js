import React from 'react';

export default class Editor extends React.Component {
    constructor() {
        super();
        this.editor = null;
        this.editorAnchor = null;
        this.onEditorChange = this.onEditorChange.bind(this);
    }

    onEditorChange() {
        this.props.onChange(this.editor.getValue());
    }

    componentDidMount() {
        if (this.editorAnchor.innerHTML !== this.props.content) {
            this.editorAnchor.innerHTML = this.props.content;
        }

        this.editor = ace.edit('editor');
        this.editor.setReadOnly(this.props.readOnly);
        this.editor.on('change', this.onEditorChange);
    }

    shouldComponentUpdate(nextProps) {
        return nextProps.content !== this.editor.getValue();
    }

    componentDidUpdate() {
        this.componentWillUnmount();
        this.componentDidMount();
    }

    componentWillUnmount() {
        this.editor.off('change', this.onEditorChange);
        this.editor.destroy();
    }

    render() {
        const preStyle = {
            width: '100%',
            height: '80%',
            margin: 0,
            clear: 'both'
        };

        return (
            <pre id="editor" style={preStyle} ref={(ref) => { this.editorAnchor = ref; } }>
                { this.props.content }
            </pre>
        );
    }
}

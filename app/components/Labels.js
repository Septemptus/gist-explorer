import React from 'react';
import TextField from 'material-ui/TextField';
import Chip from 'material-ui/Chip';
import { setLabel, unsetLabel } from '../actions/LabelActions';
import LabelStore from '../stores/LabelStore';
import UserStore from '../stores/UserStore';

const KEY_CODE_ENTER = 13;

const labelsStyle = {
    float: 'left',
    marginLeft: '10px',
    marginBottom: '15px'
};

const chipStyle = {
    display: 'flex',
    flexWrap: 'wrap'
};

export default class Labels extends React.Component {
    constructor() {
        super();
        this.state = {
            newLabel: '',
            labels: [],
            error: false
        };

        this.onLabelsChange = this.onLabelsChange.bind(this);
        this.onUserChange = this.onUserChange.bind(this);
    }

    componentWillMount() {
        LabelStore.on('change', this.onLabelsChange);
        UserStore.on('change', this.onUserChange);
    }

    componentWillUnmount() {
        LabelStore.removeListener('change', this.onLabelsChange);
        UserStore.removeListener('change', this.onUserChange);
    }

    componentDidMount() {
        this.onLabelsChange();
        this.onUserChange();
    }

    onUserChange() {
        this.setState({
            error: UserStore.hasError()
        });
    }

    onLabelsChange() {
        this.setState({
            labels: LabelStore.getLabelsForGist(this.props.gistId)
        });
    }

    onKeyDown(e) {
        if (e.keyCode === KEY_CODE_ENTER && this.state.newLabel) {
            this.addLabel(this.state.newLabel);
        }
    }

    onNewLabelChange(e) {
        this.setState({
            newLabel: e.target.value
        });
    }

    onLabelRemove(label) {
        unsetLabel(this.props.gistId, label);
    }

    addLabel(label) {
        this.setState({
            newLabel: ''
        });
        setLabel(this.props.gistId, label);
    }

    render() {
        const labels = this.state.labels.map((label, idx) => {
            return (
                <Chip
                    key={idx}
                    style={{ margin: 4 }}
                    onRequestDelete={this.onLabelRemove.bind(this, label)}
                >
                    { label }
                </Chip>
            );
        });

        if (this.state.error) {
            return (
                <div style={ labelsStyle }>
                    <p>User data could not be loaded, label functionality disabled</p>
                </div>
            );
        }

        return (
            <div style={ labelsStyle }>
                <TextField
                    value={this.state.newLabel}
                    onChange={this.onNewLabelChange.bind(this)}
                    onKeyDown={this.onKeyDown.bind(this)}
                    hintText="Add label"
                />
                <div style={ chipStyle }>
                    { labels }
                </div>
            </div>
        );
    }
}

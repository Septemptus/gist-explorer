import React from 'react';
import CircularProgress from 'material-ui/CircularProgress';

const coverStyle = {
    position: 'absolute',
    backgroundColor: 'white',
    top: '0',
    left: '0',
    zIndex: '9999',
    width: '100%',
    height: '100%'
};

const progressStyle = {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    height: '100%'
};

export default class SplashLoader extends React.Component {
    render() {
        return this.props.show ? (
            <div style={ coverStyle }>
                <div style={ progressStyle } >
                    <CircularProgress/>
                </div>
            </div>)
            : null;
    }
}
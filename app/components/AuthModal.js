import React from 'react';
import Dialog from 'material-ui/Dialog';
import AuthStore from '../stores/AuthStore';
import GitHubLogin from './GitHubLogin';

const buttonContainerStyle = {
    width: '30%',
    marginLeft: 'auto',
    marginRight: 'auto',
    textAlign: 'center'
};

const noTransitionStyle = {
    transition: 'none'
};

export default class AuthModal extends React.Component {
    constructor() {
        super();
        this.state = {
            show: !AuthStore.isAuthorized(),
            error: AuthStore.hasError()
        };

        this.onAuthStatusChange = this.onAuthStatusChange.bind(this);
    }

    componentWillMount() {
        AuthStore.on('change', this.onAuthStatusChange);
    }

    componentWillUnmount() {
        AuthStore.removeListener('change', this.onAuthStatusChange);
    }

    onAuthStatusChange() {
        this.setState({
            show: !AuthStore.isAuthorized(),
            error: AuthStore.hasError()
        });
    }

    render() {
        return (
            <Dialog
                style={ noTransitionStyle }
                contentStyle={ noTransitionStyle }
                bodyStyle={ noTransitionStyle }
                modal={true}
                open={this.state.show}
            >
                <div style={ buttonContainerStyle }>
                    { this.state.error ? <p>Authentication failed</p> : null }
                    <GitHubLogin/>
                </div>
            </Dialog>
        );
    }
}

import React from 'react';
import { Card, CardHeader } from 'material-ui/Card';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import Paper from 'material-ui/Paper';
import CircularProgress from 'material-ui/CircularProgress';
import { Row, Col } from 'react-flexbox-grid/lib/index';
import { save, addFile, deleteFile, setFile } from '../actions/GistActions';
import CurrentGistStore from '../stores/CurrentGistStore';
import Editor from './Editor';
import Toolbar from './Toolbar';
import Labels from './Labels';

const height100Percent = {
    height: '100%'
};

const selectStyle = {
    float: 'right',
    marginRight: '10px'
};

const progressStyle = {
    height: '100%',
    textAlign: 'center'
};

export default class GistPanel extends React.Component {
    constructor() {
        super();
        this.state = {
            editorContent: '',
            gist: null,
            file: null,
            files: [],
            error: false,
            loading: false,
            readOnly: false
        };

        this.onGistChange = this.onGistChange.bind(this);
    }

    componentWillMount() {
        CurrentGistStore.on('change', this.onGistChange);
    }

    componentWillUnmount() {
        CurrentGistStore.removeListener('change', this.onGistChange);
    }

    onGistChange() {
        const currentGist = CurrentGistStore.getCurrentGist();
        const files = CurrentGistStore.getFiles();
        const file = CurrentGistStore.getCurrentFile();
        const editorContent = CurrentGistStore.getContent();

        this.setState({
            file,
            files,
            editorContent,
            gist: currentGist,
            error: CurrentGistStore.hasError(),
            loading: CurrentGistStore.isLoading(),
            readOnly: CurrentGistStore.isReadOnly()
        });
    }

    selectFile(event, index, file) {
        setFile(file);
    }

    renderFiles() {
        return Object.keys(this.state.gist.files).map((file, idx) => {
            return (
                <MenuItem value={file} primaryText={file} key={idx} />
            );
        });
    }

    handleEditorChange(editorContent) {
        this.setState({
            editorContent
        });
    }

    save() {
        save(this.state.gist.id, this.state.file, this.state.editorContent);
    }

    discard() {
        this.handleEditorChange(this.state.gist.files[this.state.file].content);
    }

    addFile(filename) {
        addFile(this.state.gist.id, filename);
    }

    deleteFile() {
        deleteFile(this.state.gist.id, this.state.file);
    }

    renderPanel() {
        return (
            <Paper style={ height100Percent }>
                <Row>
                    <Col xs={12} md={5}>
                        <Labels gistId={this.state.gist.id} />
                    </Col>
                    <Col xs={12} md={7}>
                        <div style={ selectStyle }>
                            <SelectField
                                value={ this.state.file }
                                onChange={ this.selectFile.bind(this) }
                            >
                                { this.renderFiles() }
                            </SelectField>
                        </div>
                        <Toolbar
                            readOnly={ this.state.readOnly }
                            onSave={ this.save.bind(this) }
                            onDiscard={ this.discard.bind(this) }
                            onAdd={ this.addFile.bind(this) }
                            onDelete={ this.deleteFile.bind(this) }
                        />
                    </Col>
                </Row>
                <Editor
                    readOnly={ this.state.readOnly }
                    content={ this.state.editorContent }
                    onChange={ this.handleEditorChange.bind(this) }
                />
            </Paper>
        );
    }

    renderEmptyCard(content, styleOverride) {
        return (
            <Card style={ styleOverride || height100Percent }>
                <CardHeader
                    title={content}
                />
            </Card>
        );
    }

    render() {
        const emptyCard = this.renderEmptyCard('Please select a gist first.');

        const error = this.renderEmptyCard('Could not perform the operation.');

        const loader = this.renderEmptyCard(<CircularProgress/>, progressStyle);

        const panel = !this.state.gist ? emptyCard : this.renderPanel();

        return (
            <div style={ height100Percent }>
                {
                    this.state.error ?
                        error :
                        this.state.loading ?
                            loader :
                            panel
                }
            </div>
        );
    }
}
import React from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';

export default class AreYouSure extends React.Component {
    constructor() {
        super();
        this.state = {
            open: false
        };
    }

    close() {
        this.setState({
            open: false
        });
    }

    render() {
        const actions = [
            <FlatButton
                primary={ true }
                label="Yes"
                onClick={ this.props.onYes }
            />,
            <FlatButton
                secondary={ true }
                label="No"
                onClick={ this.props.onNo }
            />
        ];

        return (
            <Dialog
                title="Please confirm"
                open={ this.props.open }
                onRequestClose={ this.props.onNo }
                actions={ actions }
            >
                Are you sure?
            </Dialog>
        );
    }
}

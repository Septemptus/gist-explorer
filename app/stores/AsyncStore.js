import { EventEmitter } from 'events';

export class AsyncStore extends EventEmitter {
    constructor() {
        super();
        this.error = false;
        this.loading = false;
    }

    setError() {
        this.error = true;
    }

    hasError() {
        return this.error;
    }

    resetError() {
        this.error = false;
    }

    setLoading() {
        this.loading = true;
    }

    isLoading() {
        return this.loading;
    }

    resetLoading() {
        this.loading = false;
    }

    reload() {
        this.resetError();
        this.setLoading();
    }
}

export default AsyncStore;
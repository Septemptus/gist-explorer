import { EventEmitter } from 'events';
import dispatcher from '../dispatcher';
import UserStore from './UserStore';

const BASE_STORAGE_KEY = 'gist-explorer-ls-labels';

export class LabelStore extends EventEmitter {
    constructor() {
        super();
        this.labels = {};
    }

    getStorageKey() {
        return `${BASE_STORAGE_KEY}-${UserStore.getUsername()}`;
    }

    loadLabels() {
        this.labels = UserStore.getUsername() && JSON.parse(localStorage.getItem(this.getStorageKey())) || {};
    }

    saveLabels() {
        localStorage.setItem(this.getStorageKey(), JSON.stringify(this.labels));
    }

    getLabels() {
        return Object.keys(this.labels).sort();
    }

    getLabelsForGist(gistId) {
        return this.getLabels().filter((label) => {
             return this.labels[label].includes(gistId);
        });
    }

    hasLabels(gistId, ...names) {
        return names.reduce((hasAllLabels, name) => {
            return hasAllLabels && !!this.labels[name] && this.labels[name].includes(gistId);
        }, true);
    }

    addLabel(name) {
        this.labels[name] = [];
        this.saveLabels();
    }

    removeLabel(name) {
        delete this.labels[name];
        this.saveLabels();
    }

    setLabel(gistId, name) {
        if (!this.labels[name]) {
            this.addLabel(name);
        }

        if (!this.labels[name].includes(gistId)) {
            this.labels[name].push(gistId);
        }

        this.saveLabels();
    }

    unsetLabel(gistId, name) {
        if (this.labels[name] && this.labels[name].includes(gistId)) {
            this.labels[name] = this.labels[name].filter((storedGistId) => storedGistId != gistId);
        }

        if (this.labels[name] && this.labels[name].length === 0) {
            this.removeLabel(name);
        }

        this.saveLabels();
    }

    removeGistFromLabels(gistId) {
        this.getLabelsForGist(gistId).forEach((label) => {
            this.unsetLabel(gistId, label);
        });
    }

    handleAction(action) {
        switch(action.type) {
            case 'LOG_OUT':
            case 'USER_LOADED': {
                dispatcher.waitFor([UserStore.dispatchToken]);
                this.loadLabels();
                this.emit('change');
                break;
            }

            case 'SET_LABEL': {
                this.setLabel(action.gistId, action.name);
                this.emit('change');
                break;
            }

            case 'UNSET_LABEL': {
                this.unsetLabel(action.gistId, action.name);
                this.emit('change');
                break;
            }

            case 'GIST_DELETED': {
                this.removeGistFromLabels(action.gistId);
                this.emit('change');
                break;
            }
        }
    }
}

let labelStore = new LabelStore();
labelStore.dispatchToken = dispatcher.register(labelStore.handleAction.bind(labelStore));

export default labelStore;
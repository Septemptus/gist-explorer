import { EventEmitter } from 'events';
import dispatcher from '../dispatcher';

export class FilterStore extends EventEmitter {
    constructor() {
        super();
        this.filters = [];
    }

    getFilters() {
        return this.filters;
    }

    isOn() {
        return this.filters.length > 0;
    }

    setFilter(name) {
        if (this.filters.includes(name)) {
            this.removeFilter(name);
        } else {
            this.filters.push(name);
        }
    }

    removeFilter(name) {
        this.filters = this.filters.filter((label) => label !== name);
    }

    resetFilter() {
        this.filters = [];
    }

    handleAction(action) {
        switch(action.type) {
            case 'LOG_OUT':
            case 'LOAD_STARRED_GISTS':
            case 'LOAD_GISTS': {
                this.resetFilter();
                this.emit('change');
                break;
            }
            case 'SET_FILTER': {
                this.setFilter(action.name);
                this.emit('change');
                break;
            }
        }
    }
}

let filterStore = new FilterStore();
filterStore.dispatchToken = dispatcher.register(filterStore.handleAction.bind(filterStore));

export default filterStore;
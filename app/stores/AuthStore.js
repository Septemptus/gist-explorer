import AsyncStore from './AsyncStore';
import { authorize } from '../api/Auth';
import dispatcher from '../dispatcher';

const CLIENT_ID = 'c0d50e307111455f6ce7';
const TOKEN_KEY = 'gist-explorer-ls-token';

export class AuthStore extends AsyncStore {
    constructor() {
        super();
        this.auth = localStorage.getItem(TOKEN_KEY);
    }

    loadCode() {
        const queryMatch = window.location.search.match(/code=(.+)$/);
        return queryMatch && queryMatch[1];
    }

    getClientId() {
        return CLIENT_ID;
    }

    isAuthorized() {
        return !!this.auth;
    }

    getToken() {
        return this.auth;
    }

    setAuth(auth) {
        this.auth = auth;
        localStorage.setItem(TOKEN_KEY, this.auth);
    }

    resetAuth() {
        localStorage.removeItem(TOKEN_KEY);
        this.auth = null;
    }

    handleAction(action) {
        switch(action.type) {
            case 'AUTHORIZE': {
                const code = this.loadCode();
                if (code) {
                    this.reload();
                    authorize(code);
                }
                this.emit('change');
                break;
            }

            case 'AUTHORIZED': {
                this.setAuth(action.auth);
                this.resetLoading();
                this.emit('change');
                break;
            }

            case 'AUTHORIZE_FAILED': {
                this.resetAuth();
                this.resetLoading();
                this.setError();
                this.emit('change');
                break;
            }

            case 'LOG_OUT': {
                this.resetAuth();
                this.emit('change');
                break;
            }
        }
    }
}

let authStore = new AuthStore();
authStore.dispatchToken = dispatcher.register(authStore.handleAction.bind(authStore));

export default authStore;
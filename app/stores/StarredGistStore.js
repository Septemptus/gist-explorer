import { GistStore } from './GistStore';
import dispatcher from '../dispatcher';
import { loadStarredGists } from '../api/Gists';
import AuthStore from './AuthStore';

export class StarredGistStore extends GistStore {
    handleAction(action) {
        switch (action.type) {
            case 'LOG_OUT':
            case 'SET_FILTER':
            case 'GISTS_LOADED':
            case 'GISTS_LOAD_FAILED':
            case 'SET_LABEL': {
                super.handleAction(action);
                break;
            }

            case 'LOAD_STARRED_GISTS': {
                this.reload();
                loadStarredGists(AuthStore.getToken());
                this.emit('overtake');
                this.emit('change');
                break;
            }
        }
    }
}

let starredGistStore = new StarredGistStore();
starredGistStore.dispatchToken = dispatcher.register(starredGistStore.handleAction.bind(starredGistStore));

export default starredGistStore;
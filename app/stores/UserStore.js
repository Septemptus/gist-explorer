import AsyncStore from './AsyncStore';
import AuthStore from './AuthStore';
import dispatcher from '../dispatcher';
import { loadUser } from '../api/User';

export class UserStore extends AsyncStore {
    constructor() {
        super();
        this.userData = null;
    }

    setUser(userData) {
        this.userData = userData;
    }

    resetUser() {
        this.userData = null;
    }

    getUsername() {
        return this.userData && this.userData.login;
    }

    handleAction(action) {
        switch(action.type) {
            case 'AUTHORIZED': {
                dispatcher.waitFor([AuthStore.dispatchToken]);
                this.reload();
                loadUser(AuthStore.getToken());
                this.emit('change');
                break;
            }

            case 'USER_LOADED': {
                this.setUser(action.userData);
                this.resetLoading();
                this.emit('change');
                break;
            }

            case 'LOG_OUT': {
                this.resetUser();
                this.emit('change');
                break;
            }

            case 'USER_LOAD_FAILED': {
                this.setError();
                this.resetLoading();
                this.emit('change');
            }
        }
    }
}

let userStore = new UserStore();
userStore.dispatchToken = dispatcher.register(userStore.handleAction.bind(userStore));

export default userStore;
import dispatcher from '../dispatcher';
import { loadGists, deleteGist } from '../api/Gists';
import AsyncStore from './AsyncStore';
import AuthStore from './AuthStore';
import LabelStore from './LabelStore';
import FilterStore from './FilterStore';

export class GistStore extends AsyncStore {
    constructor() {
        super();
        this.gists = [];
        this.filteredGists = [];
        this.error = false;
        this.loading = false;
    }

    filter(gists) {
        return gists.filter((gist) => LabelStore.hasLabels(gist.id, ...FilterStore.getFilters()));
    }

    setGists(gists) {
        this.gists = gists;
        this.filteredGists = this.filter(this.gists);
    }

    getGists() {
        return this.filteredGists;
    }

    handleAction(action) {
        switch (action.type) {
            case 'GIST_CREATED':
            case 'GIST_DELETED':
            case 'LOAD_GISTS':
            case 'AUTHORIZED': {
                dispatcher.waitFor([AuthStore.dispatchToken]);
                this.reload();
                loadGists(AuthStore.getToken());
                this.emit('overtake');
                this.emit('change');
                break;
            }

            case 'LOG_OUT': {
                this.setGists([]);
                this.emit('change');
                break;
            }

            case 'SET_LABEL': {
                dispatcher.waitFor([LabelStore.dispatchToken]);
                this.setGists(this.gists);
                this.emit('change');
                break;
            }

            case 'SET_FILTER': {
                dispatcher.waitFor([FilterStore.dispatchToken]);
                this.setGists(this.gists);
                this.emit('change');
                break;
            }

            case 'GISTS_LOADED': {
                if (this.loading) {
                    this.setGists(action.gists);
                    this.resetLoading();
                    this.emit('change');
                }
                break;
            }

            case 'GISTS_LOAD_FAILED': {
                if (this.loading) {
                    this.setError();
                    this.resetLoading();
                    this.emit('change');
                }
                break;
            }

            case 'DELETE_GIST': {
                this.reload();
                deleteGist(AuthStore.getToken(), action.gistId);
                this.emit('change');
                break;
            }
        }
    }
}

let gistStore = new GistStore();
gistStore.dispatchToken = dispatcher.register(gistStore.handleAction.bind(gistStore));

export default gistStore;
import dispatcher from '../dispatcher';
import { loadGist, saveGist, createGist, deleteGist } from '../api/Gists';
import AsyncStore from './AsyncStore';
import AuthStore from './AuthStore';

export class CurrentGistStore extends AsyncStore {
    constructor() {
        super();
        this.currentGist = null;
        this.currentFile = null;
        this.content = null;
        this.readOnly = false;
    }

    setCurrentGist(gist) {
        const oldGist = this.currentGist;
        this.currentGist = gist;
        this.currentFile = oldGist && oldGist.id === gist.id && this.getFiles().includes(this.currentFile) ? this.currentFile : this.getFiles()[0];
        this.updateContent();
    }

    getCurrentGist() {
        return this.currentGist;
    }

    resetCurrentGist() {
        this.currentGist = null;
        this.currentFile = null;
        this.content = null;
    }

    getFiles() {
        return this.currentGist ? Object.keys(this.currentGist.files) : [];
    }

    setCurrentFile(file) {
        this.currentFile = file;
    }

    getCurrentFile() {
        return this.currentFile;
    }

    getContent() {
        return this.content;
    }

    updateContent() {
        this.content = this.currentGist && this.currentGist.files[this.currentFile].content;
    }

    setReadOnly() {
        this.readOnly = true;
    }

    resetReadOnly() {
        this.readOnly = false;
    }

    isReadOnly() {
        return this.readOnly;
    }

    handleAction(action) {
        switch (action.type) {
            case 'LOAD_GISTS': {
                this.resetReadOnly();
                this.resetError();
                this.resetCurrentGist();
                this.emit('change');
                break;
            }

            case 'LOAD_STARRED_GISTS': {
                this.setReadOnly();
                this.resetError();
                this.resetCurrentGist();
                this.emit('change');
                break;
            }

            case 'GIST_DELETED':
            case 'SET_FILTER':
            case 'LOG_OUT': {
                this.resetLoading();
                this.resetCurrentGist();
                this.emit('change');
                break;
            }

            case 'GIST_SAVED':
            case 'LOAD_GIST': {
                this.reload();
                loadGist(AuthStore.getToken(), action.gistId);
                this.emit('change');
                break;
            }

            case 'GIST_LOADED': {
                this.setCurrentGist(action.gist);
                this.resetLoading();
                this.emit('change');
                break;
            }

            case 'GIST_CREATE_FAILED':
            case 'GIST_DELETE_FAILED':
            case 'GIST_SAVE_FAILED':
            case 'GIST_LOAD_FAILED': {
                this.setError();
                this.resetLoading();
                this.emit('change');
                break;
            }

            case 'SAVE_GIST': {
                this.reload();
                saveGist(AuthStore.getToken(), action.gistId, action.filename, action.content);
                this.emit('change');
                break;
            }

            case 'CREATE_GIST': {
                this.reload();
                createGist(AuthStore.getToken(), action.name, action.isPrivate);
                this.emit('change');
                break;
            }

            case 'GIST_CREATED': {
                this.resetReadOnly();
                this.setCurrentGist(action.gist);
                this.resetLoading();
                this.emit('change');
                break;
            }

            case 'ADD_FILE': {
                this.reload();
                this.setCurrentFile(action.filename);
                saveGist(AuthStore.getToken(), action.gistId, action.filename, 'Edit me!');
                this.emit('change');
                break;
            }

            case 'DELETE_FILE': {
                this.reload();
                if (this.getFiles().length > 1) {
                    saveGist(AuthStore.getToken(), action.gistId, action.filename, null);
                } else {
                    deleteGist(AuthStore.getToken(), action.gistId);
                }

                this.emit('change');
                break;
            }

            case 'SET_FILE': {
                this.setCurrentFile(action.filename);
                this.updateContent();
                this.emit('change');
                break;
            }
        }
    }
}

let currentGistStore = new CurrentGistStore();
currentGistStore.dispatchToken = dispatcher.register(currentGistStore.handleAction.bind(currentGistStore));

export default currentGistStore;
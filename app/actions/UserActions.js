import dispatcher from '../dispatcher';

export function loaded(userData) {
    dispatcher.dispatch({
        type: 'USER_LOADED',
        userData
    });
}

export function loadFailed(error) {
    dispatcher.dispatch({
        type: 'USER_LOAD_FAILED',
        error
    });
}
import dispatcher from '../dispatcher';

export function loadGists() {
    dispatcher.dispatch({
        type: 'LOAD_GISTS'
    });
}

export function loadStarredGists() {
    dispatcher.dispatch({
        type: 'LOAD_STARRED_GISTS'
    });
}

export function loadGist(gistId) {
    dispatcher.dispatch({
        type: 'LOAD_GIST',
        gistId
    });
}

export function gistsLoaded(gists) {
    dispatcher.dispatch({
        type: 'GISTS_LOADED',
        gists
    });
}

export function gistLoaded(gist) {
    dispatcher.dispatch({
        type: 'GIST_LOADED',
        gist
    });
}

export function gistsLoadFailed(error) {
    dispatcher.dispatch({
        type: 'GISTS_LOAD_FAILED',
        error
    });
}

export function gistLoadFailed(error) {
    dispatcher.dispatch({
        type: 'GIST_LOAD_FAILED',
        error
    });
}

export function save(gistId, filename, content) {
    dispatcher.dispatch({
        type: 'SAVE_GIST',
        gistId,
        filename,
        content
    });
}

export function saved(gistId) {
    dispatcher.dispatch({
        type: 'GIST_SAVED',
        gistId
    });
}

export function saveFailed(error) {
    dispatcher.dispatch({
        type: 'GIST_SAVE_FAILED',
        error
    });
}

export function create(name, isPrivate) {
    dispatcher.dispatch({
        type: 'CREATE_GIST',
        name,
        isPrivate
    });
}

export function created(gist) {
    dispatcher.dispatch({
        type: 'GIST_CREATED',
        gist
    });
}

export function createFailed(error) {
    dispatcher.dispatch({
        type: 'GIST_CREATE_FAILED',
        error
    });
}

export function addFile(gistId, filename) {
    dispatcher.dispatch({
        type: 'ADD_FILE',
        gistId,
        filename
    });
}

export function deleteFile(gistId, filename) {
    dispatcher.dispatch({
        type: 'DELETE_FILE',
        gistId,
        filename
    });
}

export function setFile(filename) {
    dispatcher.dispatch({
        type: 'SET_FILE',
        filename
    });
}

export function deleteGist(gistId) {
    dispatcher.dispatch({
        type: 'DELETE_GIST',
        gistId
    });
}

export function deleted(gistId) {
    dispatcher.dispatch({
        type: 'GIST_DELETED',
        gistId
    });
}

export function deleteFailed(error) {
    dispatcher.dispatch({
        type: 'GIST_DELETE_FAILED',
        error
    });
}
import dispatcher from '../dispatcher';

export function setFilter(name) {
    dispatcher.dispatch({
        type: 'SET_FILTER',
        name
    });
}
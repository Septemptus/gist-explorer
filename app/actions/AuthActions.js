import dispatcher from '../dispatcher';

export function authorize() {
    dispatcher.dispatch({
        type: 'AUTHORIZE'
    });
}

export function authorized(auth) {
    dispatcher.dispatch({
        type: 'AUTHORIZED',
        auth
    });
}

export function failed(error) {
    dispatcher.dispatch({
        type: 'AUTHORIZE_FAILED',
        error
    });
}

export function logOut() {
    dispatcher.dispatch({
        type: 'LOG_OUT'
    });
}
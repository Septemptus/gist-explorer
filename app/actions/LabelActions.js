import dispatcher from '../dispatcher';

export function setLabel(gistId, name) {
    dispatcher.dispatch({
        type: 'SET_LABEL',
        gistId,
        name
    });
}

export function unsetLabel(gistId, name) {
    dispatcher.dispatch({
        type: 'UNSET_LABEL',
        gistId,
        name
    });
}
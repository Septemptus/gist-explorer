import React from 'react';
import ReactDOM from 'react-dom';
import Layout from './components/Layout';

import injectTapEventPlugin from 'react-tap-event-plugin';

injectTapEventPlugin();

ReactDOM.render(<Layout/>, document.getElementById('container'));
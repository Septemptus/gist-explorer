var path = require('path');

module.exports = {
    entry: './app/index.js',
    devtool: 'source-map',
    output: {
        publicPath: '/build',
        path: path.resolve(__dirname, 'build'),
        filename: 'app.min.js'
    },
    module: {
        preLoaders: [
            {
                test: /\.js$/,
                loader: 'eslint-loader',
                exclude: /node_modules/
            }
        ],
        loaders: [
            {
                test: /\.css$/,
                loader: 'style!css?modules',
                include: /flexboxgrid/
            },
            {
                test: /\.js$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
                query: {
                    presets: ['es2015', 'stage-1', 'react'],
                    plugins: ['transform-decorators-legacy']
                }
            }
        ]
    }
};
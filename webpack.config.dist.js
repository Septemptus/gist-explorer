const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const base = require('./webpack.config');

base.output.publicPath = '/';
base.plugins = [
    new HtmlWebpackPlugin({
        inject: 'body',
        template: __dirname + '/app/dist-template.ejs'
    }),
    new webpack.DefinePlugin({
        'process.env': {
            NODE_ENV: JSON.stringify('production')
        }
    }),
    new webpack.optimize.UglifyJsPlugin({
        compress: {
            warnings: false
        }
    })
];

module.exports = base;
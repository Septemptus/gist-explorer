import MockAdapter from 'axios-mock-adapter';
import { spy } from 'sinon';

import { expect } from 'chai';

import axios from 'axios';
import { loadUser, __RewireAPI__ as UserRewire } from '../../app/api/User';
import { __RewireAPI__ as RequestHelperRewire } from '../../app/api/requestHelper';

describe('api/User', () => {
    let mock;
    let headerSpy;
    let successActionSpy;
    let failActionSpy;
    let failedSpy;

    before(() => {
        // initialize spies
        headerSpy = spy();
        successActionSpy = spy();
        failActionSpy = spy();
        failedSpy = spy();

        // rewire
        RequestHelperRewire.__Rewire__('failed', failedSpy);
        UserRewire.__Rewire__('loaded', successActionSpy);
        UserRewire.__Rewire__('loadFailed', failActionSpy);
    });

    beforeEach(() => {
        headerSpy.reset();
        successActionSpy.reset();
        failActionSpy.reset();
    });

    after(() => {
        RequestHelperRewire.__ResetDependency__('failed');
        UserRewire.__ResetDependency__('loaded');
        UserRewire.__ResetDependency__('loadFailed');
    });

    describe('loadUser', () => {
        before(() => {
            // request mock
            mock = new MockAdapter(axios);
            mock.onGet(/https:\/\/api.github.com\/user/).reply((config) => {
                headerSpy(config.headers);
                return [200, { login: '123' }];
            });
        });

        after(() => {
            mock.restore();
        });

        it('should send a GET request', (done) => {
            loadUser('secret').then(done);
        });

        it('should send correct auth header', (done) => {
            loadUser('secret').then(() => {
                expect(headerSpy.lastCall.args[0].Authorization).to.equal('token secret');
                done();
            });
        });

        describe('when request is successful', () => {
            it('should call a loaded action', (done) => {
                loadUser('secret').then(() => {
                    expect(successActionSpy.called).to.equal(true);
                    done();
                });
            });

            it('should call an action with the response data', (done) => {
                loadUser('secret').then(() => {
                    expect(successActionSpy.calledWith({ login: '123'})).to.equal(true);
                    done();
                });
            });
        });

        describe('when request is unsuccessful', () => {
            before(() => {
                mock.reset();
                mock.onGet(/https:\/\/api.github.com\/user/).reply(500, 'error');
            });

            it('should call a loadFailed action', (done) => {
                loadUser('secret').then(() => {
                    expect(failActionSpy.called).to.equal(true);
                    done();
                });
            });

            it('should call an action with the error', (done) => {
                loadUser('secret').then(() => {
                    expect(failActionSpy.lastCall.args[0]).to.equal('error');
                    done();
                });
            });

            it('should call failed action when receiving a 401', (done) => {
                mock.reset();
                mock.onGet(/https:\/\/api.github.com\/user/).reply(401, 'error');
                loadUser('secret').then(() => {
                    expect(failedSpy.calledOnce).to.equal(true);
                    done();
                });
            });
        });
    });
});
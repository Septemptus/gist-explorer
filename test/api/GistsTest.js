import MockAdapter from 'axios-mock-adapter';
import { spy } from 'sinon';

import { expect } from 'chai';

import axios from 'axios';
import {
    loadGists,
    loadStarredGists,
    loadGist,
    saveGist,
    createGist,
    deleteGist,
    __RewireAPI__ as GistsRewire
} from '../../app/api/Gists';
import { __RewireAPI__ as RequestHelperRewire } from '../../app/api/requestHelper';

describe('api/Gists', () => {
    let mock;
    let headerSpy;
    let contentSpy;
    let successActionSpy;
    let failActionSpy;
    let failedSpy;

    before(() => {
        // initialize spies
        headerSpy = spy();
        contentSpy = spy();
        successActionSpy = spy();
        failActionSpy = spy();
        failedSpy = spy();

        // rewire
        RequestHelperRewire.__Rewire__('failed', failedSpy);
        GistsRewire.__Rewire__('gistsLoaded', successActionSpy);
        GistsRewire.__Rewire__('gistsLoadFailed', failActionSpy);
    });

    beforeEach(() => {
        headerSpy.reset();
        contentSpy.reset();
        successActionSpy.reset();
        failActionSpy.reset();
        failedSpy.reset();
    });

    after(() => {
        RequestHelperRewire.__ResetDependency__('failed');
        GistsRewire.__ResetDependency__('gistsLoaded');
        GistsRewire.__ResetDependency__('gistsLoadFailed');
    });

    describe('loadGists', () => {
        before(() => {
            // request mock
            mock = new MockAdapter(axios);
            mock.onGet(/https:\/\/api.github.com\/gists/).reply((config) => {
                headerSpy(config.headers);
                return [200, 'success'];
            });
        });

        after(() => {
            mock.restore();
        });

        it('should send a GET request', (done) => {
            loadGists('secret').then(done);
        });

        it('should send correct auth header', (done) => {
            loadGists('secret').then(() => {
                expect(headerSpy.lastCall.args[0].Authorization).to.equal('token secret');
                done();
            });
        });

        describe('when request is successful', () => {
            it('should call an gistsLoaded action', (done) => {
                loadGists('secret').then(() => {
                    expect(successActionSpy.called).to.equal(true);
                    done();
                });
            });

            it('should call an action with the response data', (done) => {
                loadGists('secret').then(() => {
                    expect(successActionSpy.calledWith('success')).to.equal(true);
                    done();
                });
            });
        });

        describe('when request is unsuccessful', () => {
            before(() => {
                mock.reset();
                mock.onGet(/https:\/\/api.github.com\/gists/).reply(500, 'error');
            });

            it('should call a gistsLoadFailed action', (done) => {
                loadGists('secret').then(() => {
                    expect(failActionSpy.called).to.equal(true);
                    done();
                });
            });

            it('should call an action with the error', (done) => {
                loadGists('secret').then(() => {
                    expect(failActionSpy.lastCall.args[0]).to.equal('error');
                    done();
                });
            });

            it('should call failed action when receiving a 401', (done) => {
                mock.reset();
                mock.onGet(/https:\/\/api.github.com\/gists/).reply(401, 'error');
                loadGists('secret').then(() => {
                    expect(failedSpy.calledOnce).to.equal(true);
                    done();
                });
            });
        });
    });

    describe('loadStarredGists', () => {
        before(() => {
            // request mock
            mock = new MockAdapter(axios);
            mock.onGet(/https:\/\/api.github.com\/gists\/starred/).reply((config) => {
                headerSpy(config.headers);
                return [200, 'success'];
            });
        });

        after(() => {
            mock.restore();
        });

        it('should send a GET request', (done) => {
            loadStarredGists('secret').then(done);
        });

        it('should send correct auth header', (done) => {
            loadStarredGists('secret').then(() => {
                expect(headerSpy.lastCall.args[0].Authorization).to.equal('token secret');
                done();
            });
        });

        describe('when request is successful', () => {
            it('should call an gistsLoaded action', (done) => {
                loadStarredGists('secret').then(() => {
                    expect(successActionSpy.called).to.equal(true);
                    done();
                });
            });

            it('should call an action with the response data', (done) => {
                loadStarredGists('secret').then(() => {
                    expect(successActionSpy.calledWith('success')).to.equal(true);
                    done();
                });
            });
        });

        describe('when request is unsuccessful', () => {
            before(() => {
                mock.reset();
                mock.onGet(/https:\/\/api.github.com\/gists\/starred/).reply(500, 'error');
            });

            it('should call a gistsLoadFailed action', (done) => {
                loadStarredGists('secret').then(() => {
                    expect(failActionSpy.called).to.equal(true);
                    done();
                });
            });

            it('should call an action with the error', (done) => {
                loadStarredGists('secret').then(() => {
                    expect(failActionSpy.lastCall.args[0]).to.equal('error');
                    done();
                });
            });

            it('should call failed action when receiving a 401', (done) => {
                mock.reset();
                mock.onGet(/https:\/\/api.github.com\/gists\/starred/).reply(401, 'error');
                loadStarredGists('secret').then(() => {
                    expect(failedSpy.calledOnce).to.equal(true);
                    done();
                });
            });
        });
    });

    describe('loadGist', () => {
        before(() => {
            // rewire
            GistsRewire.__Rewire__('gistLoaded', successActionSpy);
            GistsRewire.__Rewire__('gistLoadFailed', failActionSpy);
            // request mock
            mock = new MockAdapter(axios);
            mock.onGet(/https:\/\/api.github.com\/gists\/.+/).reply((config) => {
                headerSpy(config.headers);
                return [200, 'success'];
            });
        });

        after(() => {
            mock.restore();
        });

        it('should send a GET request', (done) => {
            loadGist('secret').then(done);
        });

        it('should send correct auth header', (done) => {
            loadGist('secret').then(() => {
                expect(headerSpy.lastCall.args[0].Authorization).to.equal('token secret');
                done();
            });
        });

        describe('when request is successful', () => {
            it('should call an gistLoaded action', (done) => {
                loadGist('secret').then(() => {
                    expect(successActionSpy.called).to.equal(true);
                    done();
                });
            });

            it('should call an action with the response data', (done) => {
                loadGist('secret').then(() => {
                    expect(successActionSpy.calledWith('success')).to.equal(true);
                    done();
                });
            });
        });

        describe('when request is unsuccessful', () => {
            before(() => {
                mock.reset();
                mock.onGet(/https:\/\/api.github.com\/gists\/.+/).reply(500, 'error');
            });

            it('should call a gistLoadFailed action', (done) => {
                loadGist('secret').then(() => {
                    expect(failActionSpy.called).to.equal(true);
                    done();
                });
            });

            it('should call an action with the error', (done) => {
                loadGist('secret').then(() => {
                    expect(failActionSpy.lastCall.args[0]).to.equal('error');
                    done();
                });
            });

            it('should call failed action when receiving a 401', (done) => {
                mock.reset();
                mock.onGet(/https:\/\/api.github.com\/gists\/.+/).reply(401, 'error');
                loadGist('secret').then(() => {
                    expect(failedSpy.calledOnce).to.equal(true);
                    done();
                });
            });
        });
    });

    describe('saveGist', () => {
        before(() => {
            // rewire
            GistsRewire.__Rewire__('saved', successActionSpy);
            GistsRewire.__Rewire__('saveFailed', failActionSpy);
            // request mock
            mock = new MockAdapter(axios);
            mock.onPatch(/https:\/\/api.github.com\/gists\/.+/).reply((config) => {
                contentSpy(JSON.parse(config.data));
                headerSpy(config.headers);
                return [200, {
                    id: '123'
                }];
            });
        });

        after(() => {
            mock.restore();
        });

        it('should send a PATCH request', (done) => {
            saveGist('secret', '123', '123', '123').then(done);
        });

        it('should send correct auth header', (done) => {
            saveGist('secret', '123', '123', '123').then(() => {
                expect(headerSpy.lastCall.args[0].Authorization).to.equal('token secret');
                done();
            });
        });

        it('should send an edit request with correct structure', (done) => {
            saveGist('secret', '123', '123', null).then(() => {
                expect(contentSpy.lastCall.args[0].files['123']).to.equal(null);
                done();
            });
        });

        describe('when request is successful', () => {
            it('should call a saved action', (done) => {
                saveGist('secret', '123', '123', '123').then(() => {
                    expect(successActionSpy.called).to.equal(true);
                    done();
                });
            });

            it('should call an action with the response data', (done) => {
                saveGist('secret', '123', '123', '123').then(() => {
                    expect(successActionSpy.calledWith('123')).to.equal(true);
                    done();
                });
            });
        });

        describe('when request is unsuccessful', () => {
            before(() => {
                mock.reset();
                mock.onPatch(/https:\/\/api.github.com\/gists\/.+/).reply(500, 'error');
            });

            it('should call a saveFailed action', (done) => {
                saveGist('secret', '123', '123', '123').then(() => {
                    expect(failActionSpy.called).to.equal(true);
                    done();
                });
            });

            it('should call an action with the error', (done) => {
                saveGist('secret', '123', '123', '123').then(() => {
                    expect(failActionSpy.lastCall.args[0]).to.equal('error');
                    done();
                });
            });

            it('should call failed action when receiving a 401', (done) => {
                mock.reset();
                mock.onPatch(/https:\/\/api.github.com\/gists\/.+/).reply(401, 'error');
                saveGist('secret', '123', '123', '123').then(() => {
                    expect(failedSpy.calledOnce).to.equal(true);
                    done();
                });
            });
        });
    });

    describe('createGist', () => {
        before(() => {
            // rewire
            GistsRewire.__Rewire__('created', successActionSpy);
            GistsRewire.__Rewire__('createFailed', failActionSpy);
            // request mock
            mock = new MockAdapter(axios);
            mock.onPost('https://api.github.com/gists').reply((config) => {
                headerSpy(config.headers);
                return [200, {
                    id: '123'
                }];
            });
        });

        after(() => {
            mock.restore();
        });

        it('should send a POST request', (done) => {
            createGist('secret', '123', true).then(done);
        });

        it('should send correct auth header', (done) => {
            createGist('secret', '123', true).then(() => {
                expect(headerSpy.lastCall.args[0].Authorization).to.equal('token secret');
                done();
            });
        });

        describe('when request is successful', () => {
            it('should call a created action', (done) => {
                createGist('secret', '123', true).then(() => {
                    expect(successActionSpy.called).to.equal(true);
                    done();
                });
            });

            it('should call an action with the response data', (done) => {
                createGist('secret', '123', true).then(() => {
                    expect(successActionSpy.calledWith({ id: '123' })).to.equal(true);
                    done();
                });
            });
        });

        describe('when request is unsuccessful', () => {
            before(() => {
                mock.reset();
                mock.onPost('https://api.github.com/gists').reply(500, 'error');
            });

            it('should call a createFailed action', (done) => {
                createGist('secret', '123', true).then(() => {
                    expect(failActionSpy.called).to.equal(true);
                    done();
                });
            });

            it('should call an action with the error', (done) => {
                createGist('secret', '123', true).then(() => {
                    expect(failActionSpy.lastCall.args[0]).to.equal('error');
                    done();
                });
            });

            it('should call failed action when receiving a 401', (done) => {
                mock.reset();
                mock.onPost('https://api.github.com/gists').reply(401, 'error');
                createGist('secret', '123', true).then(() => {
                    expect(failedSpy.calledOnce).to.equal(true);
                    done();
                });
            });
        });
    });

    describe('deleteGist', () => {
        before(() => {
            // rewire
            GistsRewire.__Rewire__('deleted', successActionSpy);
            GistsRewire.__Rewire__('deleteFailed', failActionSpy);
            // request mock
            mock = new MockAdapter(axios);
            mock.onDelete(/https:\/\/api.github.com\/gists\/.+/).reply((config) => {
                headerSpy(config.headers);
                return [200];
            });
        });

        after(() => {
            mock.restore();
        });

        it('should send a DELETE request', (done) => {
            deleteGist('secret', '123').then(done);
        });

        it('should send correct auth header', (done) => {
            deleteGist('secret', '123').then(() => {
                expect(headerSpy.lastCall.args[0].Authorization).to.equal('token secret');
                done();
            });
        });

        describe('when request is successful', () => {
            it('should call a deleted action', (done) => {
                deleteGist('secret', '123').then(() => {
                    expect(successActionSpy.called).to.equal(true);
                    done();
                });
            });

            it('should call an action with the gistId', (done) => {
                deleteGist('secret', '123').then(() => {
                    expect(successActionSpy.lastCall.args[0]).to.equal('123');
                    done();
                });
            });
        });

        describe('when request is unsuccessful', () => {
            before(() => {
                mock.reset();
                mock.onDelete(/https:\/\/api.github.com\/gists\/.+/).reply(500, 'error');
            });

            it('should call a deleteFailed action', (done) => {
                deleteGist('secret', '123').then(() => {
                    expect(failActionSpy.called).to.equal(true);
                    done();
                });
            });

            it('should call an action with the error', (done) => {
                deleteGist('secret', '123').then(() => {
                    expect(failActionSpy.lastCall.args[0]).to.equal('error');
                    done();
                });
            });

            it('should call failed action when receiving a 401', (done) => {
                mock.reset();
                mock.onDelete(/https:\/\/api.github.com\/gists\/.+/).reply(401, 'error');
                deleteGist('secret', '123').then(() => {
                    expect(failedSpy.calledOnce).to.equal(true);
                    done();
                });
            });
        });
    });
});
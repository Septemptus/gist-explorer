import MockAdapter from 'axios-mock-adapter';
import { spy } from 'sinon';

import { expect } from 'chai';

import axios from 'axios';
import { authorize, __RewireAPI__ as AuthRewire } from '../../app/api/Auth';

describe('api/Auth', () => {
    describe('authorize', () => {
        let mock;
        let successActionSpy;
        let failActionSpy;

        before(() => {
            // initialize spies
            successActionSpy = spy();
            failActionSpy = spy();

            // rewire
            AuthRewire.__Rewire__('authorized', successActionSpy);
            AuthRewire.__Rewire__('failed', failActionSpy);

            // request mock
            mock = new MockAdapter(axios);
            mock.onGet(/\/login\/.+/).reply(200, 'success');
        });

        after(() => {
            AuthRewire.__ResetDependency__('authorized');
            AuthRewire.__ResetDependency__('failed');
            mock.restore();
        });

        it('should send a POST request', (done) => {
            authorize(123).then(done);
        });

        describe('when request is successful', () => {
            it('should call an authorized action', (done) => {
                authorize(123).then(() => {
                    expect(successActionSpy.called).to.equal(true);
                    done();
                });
            });

            it('should call an action with the response data', (done) => {
                authorize(123).then(() => {
                    expect(successActionSpy.calledWith('success')).to.equal(true);
                    done();
                });
            });
        });

        describe('when request is unsuccessful', () => {
            before(() => {
                mock.reset();
                mock.onGet(/\/login\/.+/).reply(500);
            });

            it('should call a failed action', (done) => {
                authorize(123).then(() => {
                    expect(failActionSpy.called).to.equal(true);
                    done();
                });
            });

            it('should call an action with the error', (done) => {
                authorize(123).then(() => {
                    expect(failActionSpy.lastCall.args[0].message).to.equal('Request failed with status code 500');
                    done();
                });
            });
        });
    });
});
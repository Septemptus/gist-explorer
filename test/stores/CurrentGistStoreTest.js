import { spy } from 'sinon';

import { expect } from 'chai';

import { CurrentGistStore, __RewireAPI__ as CurrentGistStoreRewire } from '../../app/stores/CurrentGistStore';
import dispatcher from '../../app/dispatcher';
import {
    loadGists,
    loadStarredGists,
    loadGist,
    gistLoaded,
    gistLoadFailed,
    save,
    saved,
    saveFailed,
    create,
    created,
    createFailed,
    deleted,
    deleteFailed,
    addFile,
    deleteFile,
    setFile
} from '../../app/actions/GistActions';
import { setFilter } from '../../app/actions/FilterActions';
import { logOut } from '../../app/actions/AuthActions';

describe('stores/CurrentGistStore', () => {
    let currentGistStore;

    beforeEach(() => {
        currentGistStore = new CurrentGistStore();
        currentGistStore.dispatchToken = dispatcher.register(currentGistStore.handleAction.bind(currentGistStore));
    });

    afterEach(() => {
        dispatcher.unregister(currentGistStore.dispatchToken);
    });

    it('should be created with no gist', () => {
        expect(currentGistStore.getCurrentGist()).to.equal(null);
    });

    it('should be created with no current file', () => {
        expect(currentGistStore.getCurrentFile()).to.equal(null);
    });

    it('should be created with no content', () => {
        expect(currentGistStore.getContent()).to.equal(null);
    });

    it('should be created as editable', () => {
        expect(currentGistStore.isReadOnly()).to.equal(false);
    });

    it('should be possible to set current gist', () => {
        currentGistStore.setCurrentGist({
            id: '123',
            files: {
                'x': {
                    content: '1'
                }
            }
        });
    });

    it('should set current file when updating gist', () => {
        currentGistStore.setCurrentGist({
            id: '123',
            files: {
                'x': {
                    content: '1'
                }
            }
        });
        expect(currentGistStore.getCurrentFile()).not.to.equal(null);
    });

    it('should set current file to first file if no previous file was defined', () => {
        currentGistStore.setCurrentGist({
            id: '123',
            files: {
                'x': {
                    content: '1'
                }
            }
        });
        expect(currentGistStore.getCurrentFile()).to.equal('x');
    });

    it('should set current file to previous defined if such is available', () => {
        currentGistStore.setCurrentGist({
            id: '123',
            files: {
                'x': {
                    content: '1'
                },
                'y' : {
                    content: '321'
                }
            }
        });
        currentGistStore.setCurrentFile('y');
        currentGistStore.setCurrentGist({
            id: '123',
            files: {
                'x': {
                    content: '1'
                },
                'y' : {
                    content: '321'
                }
            }
        });
        expect(currentGistStore.getCurrentFile()).to.equal('y');
    });

    it('should set content when updating current gist', () => {
        currentGistStore.setCurrentGist({
            id: '123',
            files: {
                'x': {
                    content: '1'
                },
                'y' : {
                    content: '321'
                }
            }
        });
        expect(currentGistStore.getContent()).to.equal('1');
    });

    it('should be possible to retrieve currentGist', () => {
        currentGistStore.setCurrentGist({
            id: '123',
            files: {
                'x': {
                    content: '1'
                }
            }
        });
        expect(currentGistStore.getCurrentGist()).to.deep.equal({
            id: '123',
            files: {
                'x': {
                    content: '1'
                }
            }
        });
    });

    it('should be possible to reset current gist', () => {
        currentGistStore.setCurrentGist({
            id: '123',
            files: {
                'x': {
                    content: '1'
                },
                'y' : {
                    content: '321'
                }
            }
        });
        currentGistStore.resetCurrentGist();
        expect(currentGistStore.getCurrentGist()).to.equal(null);
    });

    it('should reset current file when resetting gist', () => {
        currentGistStore.setCurrentGist({
            id: '123',
            files: {
                'x': {
                    content: '1'
                },
                'y' : {
                    content: '321'
                }
            }
        });
        currentGistStore.resetCurrentGist();
        expect(currentGistStore.getCurrentFile()).to.equal(null);
    });

    it('should reset content when resetting gist', () => {
        currentGistStore.setCurrentGist({
            id: '123',
            files: {
                'x': {
                    content: '1'
                },
                'y' : {
                    content: '321'
                }
            }
        });
        currentGistStore.resetCurrentGist();
        expect(currentGistStore.getContent()).to.equal(null);
    });

    it('should be possible to set read only state', () => {
        currentGistStore.setReadOnly();
        expect(currentGistStore.isReadOnly()).to.equal(true);
    });

    it('should be possible to reset read only state', () => {
        currentGistStore.setReadOnly();
        currentGistStore.resetReadOnly();
        expect(currentGistStore.isReadOnly()).to.equal(false);
    });

    it('should be possible to get files', () => {
        currentGistStore.setCurrentGist({
            id: '123',
            files: {
                'x': {
                    content: '1'
                },
                'y' : {
                    content: '321'
                }
            }
        });
        expect(currentGistStore.getFiles()).to.deep.equal(['x', 'y']);
    });

    it('should be possible to set current file', () => {
        currentGistStore.setCurrentFile('xyz');
    });

    it('should be possible to get current file', () => {
        currentGistStore.setCurrentFile('xyz');
        expect(currentGistStore.getCurrentFile()).to.equal('xyz');
    });

    it('should be possible to get content', () => {
        currentGistStore.getContent();
    });

    it('should be possible to update content', () => {
        currentGistStore.setCurrentGist({
            id: '123',
            files: {
                'x': {
                    content: '1'
                },
                'y' : {
                    content: '321'
                }
            }
        });
        currentGistStore.setCurrentFile('y');
        currentGistStore.updateContent();
        expect(currentGistStore.getContent()).to.equal('321');
    });

    describe('starting starred gist load action', () => {
        it('should fire change event', () => {
            const changeSpy = spy();
            currentGistStore.on('change', changeSpy);
            loadStarredGists();
            expect(changeSpy.calledOnce).to.equal(true);
        });

        it('should reset current gist', () => {
            spy(currentGistStore, 'resetCurrentGist');
            loadStarredGists();
            expect(currentGistStore.resetCurrentGist.calledOnce).to.equal(true);
            currentGistStore.resetCurrentGist.restore();
        });

        it('should mark the current gist as read only', () => {
            currentGistStore.resetReadOnly();
            loadStarredGists();
            expect(currentGistStore.isReadOnly()).to.equal(true);
        });

        it('should reset error', () => {
            currentGistStore.setError();
            loadStarredGists();
            expect(currentGistStore.hasError()).to.equal(false);
        });
    });

    describe('starting gist load action', () => {
        it('should fire change event', () => {
            const changeSpy = spy();
            currentGistStore.on('change', changeSpy);
            loadGists();
            expect(changeSpy.calledOnce).to.equal(true);
        });

        it('should reset current gist', () => {
            spy(currentGistStore, 'resetCurrentGist');
            loadGists();
            expect(currentGistStore.resetCurrentGist.calledOnce).to.equal(true);
            currentGistStore.resetCurrentGist.restore();
        });

        it('should mark the current gist as editable', () => {
            currentGistStore.setReadOnly();
            loadGists();
            expect(currentGistStore.isReadOnly()).to.equal(false);
        });

        it('should reset error', () => {
            currentGistStore.setError();
            loadGists();
            expect(currentGistStore.hasError()).to.equal(false);
        });
    });

    describe('gist deleted action', () => {
        it('should fire change event', () => {
            const changeSpy = spy();
            currentGistStore.on('change', changeSpy);
            deleted('123');
            expect(changeSpy.calledOnce).to.equal(true);
        });

        it('should reset current gist', () => {
            spy(currentGistStore, 'resetCurrentGist');
            deleted('123');
            expect(currentGistStore.resetCurrentGist.calledOnce).to.equal(true);
            currentGistStore.resetCurrentGist.restore();
        });
    });

    describe('add file action', () => {
        let saveSpy;

        beforeEach(() => {
            saveSpy = spy();
            CurrentGistStoreRewire.__Rewire__('saveGist', saveSpy);
        });

        afterEach(() => {
            CurrentGistStoreRewire.__ResetDependency__('saveGist');
        });

        it('should fire change event', () => {
            const changeSpy = spy();
            currentGistStore.on('change', changeSpy);
            addFile('123', 'd');
            expect(changeSpy.calledOnce).to.equal(true);
        });

        it('should reset error and loader', () => {
            spy(currentGistStore, 'reload');
            addFile('123', 'd');
            expect(currentGistStore.reload.calledOnce).to.equal(true);
            currentGistStore.reload.restore();
        });

        it('should set current file to the new file', () => {
            addFile('123', 'd');
            expect(currentGistStore.getCurrentFile()).to.equal('d');
        });

        it('should call api', () => {
            addFile('123', 'd');
            expect(saveSpy.calledOnce).to.equal(true);
        });
    });

    describe('delete file action', () => {
        let saveSpy,
            deleteSpy;

        beforeEach(() => {
            saveSpy = spy();
            deleteSpy = spy();
            CurrentGistStoreRewire.__Rewire__('saveGist', saveSpy);
            CurrentGistStoreRewire.__Rewire__('deleteGist', deleteSpy);
        });

        afterEach(() => {
            CurrentGistStoreRewire.__ResetDependency__('saveGist');
            CurrentGistStoreRewire.__ResetDependency__('deleteGist');
        });

        it('should fire change event', () => {
            const changeSpy = spy();
            currentGistStore.on('change', changeSpy);
            deleteFile('123', 'd');
            expect(changeSpy.calledOnce).to.equal(true);
        });

        it('should reset error and loader', () => {
            spy(currentGistStore, 'reload');
            deleteFile('123', 'd');
            expect(currentGistStore.reload.calledOnce).to.equal(true);
            currentGistStore.reload.restore();
        });

        it('given more than one file in pool should call edit api call', () => {
            currentGistStore.setCurrentGist({
                id: '123',
                files: {
                    'x': {
                        content: '1'
                    },
                    'y' : {
                        content: '321'
                    }
                }
            });
            deleteFile('123', 'x');
            expect(saveSpy.calledOnce).to.equal(true);
        });

        it('given one file in pool should call delete api call', () => {
            currentGistStore.setCurrentGist({
                id: '123',
                files: {
                    'x': {
                        content: '1'
                    }
                }
            });
            deleteFile('123', 'x');
            expect(deleteSpy.calledOnce).to.equal(true);
        });
    });

    describe('set file action', () => {
        beforeEach(() => {
            currentGistStore.setCurrentGist({
                id: '123',
                files: {
                    'x': {
                        content: '1'
                    },
                    'y' : {
                        content: '321'
                    }
                }
            });
        });

        it('should fire change event', () => {
            const changeSpy = spy();
            currentGistStore.on('change', changeSpy);
            setFile('y');
            expect(changeSpy.calledOnce).to.equal(true);
        });

        it('should set current file', () => {
            setFile('y');
            expect(currentGistStore.getCurrentFile()).to.equal('y');
        });

        it('should update content', () => {
            setFile('y');
            expect(currentGistStore.getContent()).to.equal('321');
        });
    });

    describe('set filter action', () => {
        it('should fire change event', () => {
            const changeSpy = spy();
            currentGistStore.on('change', changeSpy);
            setFilter('123');
            expect(changeSpy.calledOnce).to.equal(true);
        });

        it('should reset current gist', () => {
            spy(currentGistStore, 'resetCurrentGist');
            setFilter('123');
            expect(currentGistStore.resetCurrentGist.calledOnce).to.equal(true);
            currentGistStore.resetCurrentGist.restore();
        });
    });

    describe('log out action', () => {
        it('should fire change event', () => {
            const changeSpy = spy();
            currentGistStore.on('change', changeSpy);
            logOut();
            expect(changeSpy.calledOnce).to.equal(true);
        });

        it('should reset current gist', () => {
            spy(currentGistStore, 'resetCurrentGist');
            logOut();
            expect(currentGistStore.resetCurrentGist.calledOnce).to.equal(true);
            currentGistStore.resetCurrentGist.restore();
        });
    });

    describe('gist saved action', () => {
        let gistLoadSpy;

        beforeEach(() => {
            gistLoadSpy = spy();
            CurrentGistStoreRewire.__Rewire__('loadGist', gistLoadSpy);
        });

        afterEach(() => {
            CurrentGistStoreRewire.__ResetDependency__('loadGist');
        });

        it('should fire change event', () => {
            const changeSpy = spy();
            currentGistStore.on('change', changeSpy);
            saved('123');
            expect(changeSpy.calledOnce).to.equal(true);
        });

        it('should reset error and loader', () => {
            spy(currentGistStore, 'reload');
            saved('123');
            expect(currentGistStore.reload.calledOnce).to.equal(true);
            currentGistStore.reload.restore();
        });

        it('should call api', () => {
            saved('123');
            expect(gistLoadSpy.calledOnce).to.equal(true);
        });
    });

    describe('gist load action', () => {
        let gistLoadSpy;

        beforeEach(() => {
            gistLoadSpy = spy();
            CurrentGistStoreRewire.__Rewire__('loadGist', gistLoadSpy);
        });

        afterEach(() => {
            CurrentGistStoreRewire.__ResetDependency__('loadGist');
        });

        it('should fire change event', () => {
            const changeSpy = spy();
            currentGistStore.on('change', changeSpy);
            loadGist('123');
            expect(changeSpy.calledOnce).to.equal(true);
        });

        it('should reset error and loader', () => {
            spy(currentGistStore, 'reload');
            loadGist('123');
            expect(currentGistStore.reload.calledOnce).to.equal(true);
            currentGistStore.reload.restore();
        });

        it('should call api', () => {
            loadGist('123');
            expect(gistLoadSpy.calledOnce).to.equal(true);
        });
    });

    describe('gist loaded action', () => {
        it('should fire change event', () => {
            const changeSpy = spy();
            currentGistStore.on('change', changeSpy);
            gistLoaded({
                id: '123',
                files: {
                    'x': {
                        content: '1'
                    }
                }
            });
            expect(changeSpy.calledOnce).to.equal(true);
        });

        it('should set current gist', () => {
            gistLoaded({
                id: '123',
                files: {
                    'x': {
                        content: '1'
                    }
                }
            });
            expect(currentGistStore.getCurrentGist()).to.deep.equal({
                id: '123',
                files: {
                    'x': {
                        content: '1'
                    }
                }
            });
        });

        it('should remove the loader', () => {
            currentGistStore.setLoading();
            gistLoaded({
                id: '123',
                files: {
                    'x': {
                        content: '1'
                    }
                }
            });
            expect(currentGistStore.isLoading()).to.equal(false);
        });
    });

    describe('gist load failed action', () => {
        it('should fire change event', () => {
            const changeSpy = spy();
            currentGistStore.on('change', changeSpy);
            gistLoadFailed('error');
            expect(changeSpy.calledOnce).to.equal(true);
        });

        it('should set error', () => {
            gistLoadFailed('error');
            expect(currentGistStore.hasError()).to.equal(true);
        });

        it('should remove the loader', () => {
            currentGistStore.setLoading();
            gistLoadFailed('error');
            expect(currentGistStore.isLoading()).to.equal(false);
        });
    });

    describe('gist save failed action', () => {
        it('should fire change event', () => {
            const changeSpy = spy();
            currentGistStore.on('change', changeSpy);
            saveFailed('error');
            expect(changeSpy.calledOnce).to.equal(true);
        });

        it('should set error', () => {
            saveFailed('error');
            expect(currentGistStore.hasError()).to.equal(true);
        });

        it('should remove the loader', () => {
            currentGistStore.setLoading();
            saveFailed('error');
            expect(currentGistStore.isLoading()).to.equal(false);
        });
    });

    describe('gist delete failed action', () => {
        it('should fire change event', () => {
            const changeSpy = spy();
            currentGistStore.on('change', changeSpy);
            deleteFailed('error');
            expect(changeSpy.calledOnce).to.equal(true);
        });

        it('should set error', () => {
            deleteFailed('error');
            expect(currentGistStore.hasError()).to.equal(true);
        });

        it('should remove the loader', () => {
            currentGistStore.setLoading();
            deleteFailed('error');
            expect(currentGistStore.isLoading()).to.equal(false);
        });
    });

    describe('save gist action', () => {
        let saveSpy;

        beforeEach(() => {
            saveSpy = spy();
            CurrentGistStoreRewire.__Rewire__('saveGist', saveSpy);
        });

        afterEach(() => {
            CurrentGistStoreRewire.__ResetDependency__('saveGist');
        });

        it('should fire change event', () => {
            const changeSpy = spy();
            currentGistStore.on('change', changeSpy);
            save('123', '123', '123');
            expect(changeSpy.calledOnce).to.equal(true);
        });

        it('should reset error and loader', () => {
            spy(currentGistStore, 'reload');
            save('123', '123', '123');
            expect(currentGistStore.reload.calledOnce).to.equal(true);
            currentGistStore.reload.restore();
        });

        it('should call api', () => {
            save('123', '123', '123');
            expect(saveSpy.calledOnce).to.equal(true);
        });
    });

    describe('create gist action', () => {
        let createSpy;

        beforeEach(() => {
            createSpy = spy();
            CurrentGistStoreRewire.__Rewire__('createGist', createSpy);
        });

        afterEach(() => {
            CurrentGistStoreRewire.__ResetDependency__('createGist');
        });

        it('should fire change event', () => {
            const changeSpy = spy();
            currentGistStore.on('change', changeSpy);
            create('test.js', true);
            expect(changeSpy.calledOnce).to.equal(true);
        });

        it('should reset errors and loader', () => {
            spy(currentGistStore, 'reload');
            create('test.js', true);
            expect(currentGistStore.reload.calledOnce).to.equal(true);
            currentGistStore.reload.restore();
        });

        it('should call api', () => {
            create('test.js', true);
            expect(createSpy.calledOnce).to.equal(true);
        });
    });

    describe('gist created action', () => {
        it('should fire change event', () => {
            const changeSpy = spy();
            currentGistStore.on('change', changeSpy);
            created({
                id: '123',
                files: {
                    'x': {
                        content: '1'
                    }
                }
            });
            expect(changeSpy.calledOnce).to.equal(true);
        });

        it('should set the newly created gist as current', () => {
            created({
                id: '123',
                files: {
                    'x': {
                        content: '1'
                    }
                }
            });
            expect(currentGistStore.getCurrentGist()).to.deep.equal({
                id: '123',
                files: {
                    'x': {
                        content: '1'
                    }
                }
            });
        });

        it('should reset read only state', () => {
            currentGistStore.setReadOnly();
            created({
                id: '123',
                files: {
                    'x': {
                        content: '1'
                    }
                }
            });
            expect(currentGistStore.isReadOnly()).to.equal(false);
        });

        it('should remove the loader', () => {
            currentGistStore.setLoading();
            created({
                id: '123',
                files: {
                    'x': {
                        content: '1'
                    }
                }
            });
            expect(currentGistStore.isLoading()).to.equal(false);
        });
    });

    describe('gist creation failed action', () => {
        it('should fire change event', () => {
            const changeSpy = spy();
            currentGistStore.on('change', changeSpy);
            createFailed('error');
            expect(changeSpy.calledOnce).to.equal(true);
        });

        it('should set error', () => {
            createFailed('error');
            expect(currentGistStore.hasError()).to.equal(true);
        });

        it('should remove the loader', () => {
            currentGistStore.setLoading();
            createFailed('error');
            expect(currentGistStore.isLoading()).to.equal(false);
        });
    });
});

import { expect } from 'chai';

import AsyncStore from '../../app/stores/AsyncStore';

describe('stores/AsyncStore', () => {
    let asyncStore;

    beforeEach(() => {
        asyncStore = new AsyncStore();
    });

    it('should be created with no error', () => {
        expect(asyncStore.hasError()).to.equal(false);
    });

    it('should be created with no loader', () => {
        expect(asyncStore.isLoading()).to.equal(false);
    });

    it('should be possible to set error', () => {
        asyncStore.setError();
        expect(asyncStore.hasError()).to.equal(true);
    });

    it('should be possible to reset error', () => {
        asyncStore.setError();
        asyncStore.resetError();
        expect(asyncStore.hasError()).to.equal(false);
    });

    it('should be possible to set loader', () => {
        asyncStore.setLoading();
        expect(asyncStore.isLoading()).to.equal(true);
    });

    it('should be possible to reset loader', () => {
        asyncStore.setLoading();
        asyncStore.resetLoading();
        expect(asyncStore.isLoading()).to.equal(false);
    });
});

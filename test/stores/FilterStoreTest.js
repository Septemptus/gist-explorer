import { spy } from 'sinon';
import { expect } from 'chai';

import { FilterStore } from '../../app/stores/FilterStore';
import dispatcher from '../../app/dispatcher';
import { setFilter } from '../../app/actions/FilterActions';
import { logOut } from '../../app/actions/AuthActions';
import { loadGists, loadStarredGists} from '../../app/actions/GistActions';

describe('stores/FilterStore', () => {
    let filterStore;

    beforeEach(() => {
        filterStore = new FilterStore();
        filterStore.dispatchToken = dispatcher.register(filterStore.handleAction.bind(filterStore));
    });

    afterEach(() => {
        dispatcher.unregister(filterStore.dispatchToken);
    });

    it('should be created with no active filters', () => {
         expect(filterStore.getFilters()).to.deep.equal([]);
    });

    it('should indicate whether filters are active (ON)', () => {
        filterStore.setFilter('xyz');
        expect(filterStore.isOn()).to.equal(true);
    });

    it('should indicate whether filters are active (OFF)', () => {
        expect(filterStore.isOn()).to.equal(false);
    });

    it('should set a filter to active if it was not active before', () => {
        filterStore.setFilter('xyz');
        expect(filterStore.getFilters()).to.deep.equal(['xyz']);
    });

    it('should set a filter to inactive if it was active before', () => {
        filterStore.setFilter('xyz');
        filterStore.setFilter('xyz');
        expect(filterStore.getFilters()).to.deep.equal([]);
    });

    it('should react to setFilter action', () => {
        setFilter('abc');
        expect(filterStore.getFilters()).to.deep.equal(['abc']);
    });

    describe('log out action', () => {
        it('should fire a change event', () => {
            const changeSpy = spy();
            filterStore.on('change', changeSpy);
            logOut();
            expect(changeSpy.called).to.equal(true);
        });

        it('should reset filter', () => {
            filterStore.setFilter('123');
            logOut();
            expect(filterStore.getFilters()).to.deep.equal([]);
        });
    });

    describe('load gists action', () => {
        it('should fire a change event', () => {
            const changeSpy = spy();
            filterStore.on('change', changeSpy);
            loadGists();
            expect(changeSpy.called).to.equal(true);
        });

        it('should reset filter', () => {
            filterStore.setFilter('123');
            loadGists();
            expect(filterStore.getFilters()).to.deep.equal([]);
        });
    });

    describe('load starred gists action', () => {
        it('should fire a change event', () => {
            const changeSpy = spy();
            filterStore.on('change', changeSpy);
            loadStarredGists();
            expect(changeSpy.called).to.equal(true);
        });

        it('should reset filter', () => {
            filterStore.setFilter('123');
            loadStarredGists();
            expect(filterStore.getFilters()).to.deep.equal([]);
        });
    });

    describe('set filter action', () => {
        it('should fire a change event', () => {
            const changeSpy = spy();
            filterStore.on('change', changeSpy);
            setFilter('123');
            expect(changeSpy.called).to.equal(true);
        });

        it('should set filter', () => {
            setFilter('123');
            expect(filterStore.getFilters()).to.deep.equal(['123']);
        });
    });
});
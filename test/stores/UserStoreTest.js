import { spy } from 'sinon';
import { expect } from 'chai';

import { UserStore, __RewireAPI__ as UserStoreRewire } from '../../app/stores/UserStore';
import dispatcher from '../../app/dispatcher';
import { authorized, logOut } from '../../app/actions/AuthActions';
import { loaded, loadFailed } from '../../app/actions/UserActions';

describe('stores/UserStore', () => {
    let userStore;

    beforeEach(() => {
        userStore = new UserStore();
        userStore.dispatchToken = dispatcher.register(userStore.handleAction.bind(userStore));
    });

    afterEach(() => {
        dispatcher.unregister(userStore.dispatchToken);
    });

    it('should be created with no user', () => {
        expect(userStore.getUsername()).to.equal(null);
    });

    it('should allow setting user', () => {
        userStore.setUser({
            login: '321'
        });
        expect(userStore.getUsername()).to.equal('321');
    });

    it('should allow resetting user', () => {
        userStore.setUser({
            login: '321'
        });
        userStore.resetUser();
        expect(userStore.getUsername()).to.equal(null);
    });

    describe('authorized action', () => {
        let loadUserSpy;

        beforeEach(() => {
            loadUserSpy = spy();
            UserStoreRewire.__Rewire__('loadUser', loadUserSpy);
        });

        afterEach(() => {
            UserStoreRewire.__ResetDependency__('loadUser');
        });

        it('should fire a change event', () => {
            const changeSpy = spy();
            userStore.on('change', changeSpy);
            authorized('123');
            expect(changeSpy.called).to.equal(true);
        });

        it('should reset error and loader', () => {
            spy(userStore, 'reload');
            authorized('123');
            expect(userStore.reload.calledOnce).to.equal(true);
            userStore.reload.restore();
        });

        it('should call api', () => {
            authorized('123');
            expect(loadUserSpy.calledOnce).to.equal(true);
        });
    });

    describe('log out action', () => {
        it('should fire a change event', () => {
            const changeSpy = spy();
            userStore.on('change', changeSpy);
            logOut();
            expect(changeSpy.called).to.equal(true);
        });

        it('should reset user', () => {
            spy(userStore, 'resetUser');
            logOut();
            expect(userStore.resetUser.calledOnce).to.equal(true);
            userStore.resetUser.restore();
        });
    });

    describe('user loaded action', () => {
        it('should fire a change event', () => {
            const changeSpy = spy();
            userStore.on('change', changeSpy);
            loaded({ login: '123' });
            expect(changeSpy.called).to.equal(true);
        });

        it('should set user', () => {
            spy(userStore, 'setUser');
            loaded({ login: '123' });
            expect(userStore.setUser.calledOnce).to.equal(true);
            userStore.setUser.restore();
        });

        it('should reset loader', () => {
            userStore.setLoading();
            loaded({ login: '123' });
            expect(userStore.isLoading()).to.equal(false);
        });
    });

    describe('user load failed action', () => {
        it('should fire a change event', () => {
            const changeSpy = spy();
            userStore.on('change', changeSpy);
            loadFailed('error');
            expect(changeSpy.called).to.equal(true);
        });

        it('should set error', () => {
            loadFailed('error');
            expect(userStore.hasError()).to.equal(true);
        });

        it('should reset loader', () => {
            userStore.setLoading();
            loadFailed('error');
            expect(userStore.isLoading()).to.equal(false);
        });
    });
});
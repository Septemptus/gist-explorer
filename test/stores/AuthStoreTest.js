import { spy, stub } from 'sinon';

import { expect } from 'chai';

import { AuthStore, __RewireAPI__ as AuthStoreRewire } from '../../app/stores/AuthStore';
import dispatcher from '../../app/dispatcher';
import { authorize, authorized, logOut, failed } from '../../app/actions/AuthActions';

describe('stores/AuthStore', () => {
    let authStore;

    beforeEach(() => {
        localStorage.removeItem('gist-explorer-ls-token');
        authStore = new AuthStore();
        authStore.dispatchToken = dispatcher.register(authStore.handleAction.bind(authStore));
    });

    afterEach(() => {
        dispatcher.unregister(authStore.dispatchToken);
    });

    it('should be created with no auth data if there is no stored token', () => {
        expect(authStore.getToken()).to.be.null;
    });

    it('should be created with auth data if there is a stored token', () => {
        localStorage.setItem('gist-explorer-ls-token', '9876');
        expect(new AuthStore().getToken()).to.equal('9876');
    });

    it('should be possible to load code', () => {
        window.history.replaceState(null, null, '?code=666');
        expect(authStore.loadCode()).to.equal('666');
        window.history.replaceState(null, null, '?');
    });

    it('should be possible to get client id', () => {
        expect(authStore.getClientId()).to.be.ok;
    });

    it('should be possible to set auth data', () => {
        authStore.setAuth(123);
    });

    it('should store the auth data in local storage on set', () => {
        authStore.setAuth(123);
        expect(localStorage.getItem('gist-explorer-ls-token')).to.equal('123');
    });

    it('should be possible to retrieve auth token', () => {
        authStore.setAuth(123);
        expect(authStore.getToken()).to.equal(123);
    });

    it('should be possible to reset the auth data', () => {
        authStore.setAuth(123);
        authStore.resetAuth();
        expect(authStore.getToken()).to.be.null;
    });

    it('should reset localStorage data on reset', () => {
        localStorage.setItem('gist-explorer-ls-token', 888);
        authStore.resetAuth();
        expect(localStorage.getItem('gist-explorer-ls-token')).to.equal(null);
    });

    it('should be possible to check if authorization is set (ON)', () => {
        authStore.setAuth(123);
        expect(authStore.isAuthorized()).to.equal(true);
    });

    it('should be possible to check if authorization is set (OFF)', () => {
        expect(authStore.isAuthorized()).to.equal(false);
    });

    describe('authorize action', () => {
        let authorizeSpy;

        beforeEach(() => {
            authorizeSpy = spy();
            AuthStoreRewire.__Rewire__('authorize', authorizeSpy);
        });

        afterEach(() => {
            AuthStoreRewire.__ResetDependency__('authorize');
        });

        it('should fire change event', () => {
            const changeSpy = spy();
            authStore.on('change', changeSpy);
            authorize('123');
            expect(changeSpy.calledOnce).to.equal(true);
        });

        describe('given a code in URL', () => {
            beforeEach(() => {
                stub(authStore, 'loadCode').returns('1234');
            });

            it('should reset errors and loaders', () => {
                spy(authStore, 'reload');
                authorize('123');
                expect(authStore.reload.calledOnce).to.equal(true);
                authStore.reload.restore();
            });

            it('should call api', () => {
                authorize('123');
                expect(authorizeSpy.calledOnce).to.equal(true);
            });
        });

        describe('given no code in URL', () => {
            it('should not reset errors and loaders', () => {
                spy(authStore, 'reload');
                authorize('123');
                expect(authStore.reload.called).to.equal(false);
                authStore.reload.restore();
            });

            it('should not call api', () => {
                authorize('123');
                expect(authorizeSpy.called).to.equal(false);
            });
        });
    });

    describe('authorized action', () => {
        it('should fire change event', () => {
            const changeSpy = spy();
            authStore.on('change', changeSpy);
            authorized('123');
            expect(changeSpy.calledOnce).to.equal(true);
        });

        it('should set the auth object', () => {
            authorized('123');
            expect(authStore.getToken()).to.equal('123');
        });

        it('should remove the loader', () => {
            authorized('123');
            expect(authStore.isLoading()).to.equal(false);
        });
    });

    describe('authorization failed action', () => {
        it('should fire change event', () => {
            const changeSpy = spy();
            authStore.on('change', changeSpy);
            failed('error');
            expect(changeSpy.calledOnce).to.equal(true);
        });

        it('should remove the loader', () => {
            failed('error');
            expect(authStore.isLoading()).to.equal(false);
        });

        it('should set error', () => {
            failed('error');
            expect(authStore.hasError()).to.equal(true);
        });

        it('should reset auth info', () => {
            authStore.setAuth('123');
            failed('error');
            expect(authStore.isAuthorized()).to.equal(false);
        });
    });

    describe('log out action', () => {
        it('should fire change event', () => {
            const changeSpy = spy();
            authStore.on('change', changeSpy);
            logOut();
            expect(changeSpy.calledOnce).to.equal(true);
        });

        it('should reset the auth object', () => {
            authStore.setAuth('321');
            logOut();
            expect(authStore.getToken()).to.equal(null);
        });
    });
});
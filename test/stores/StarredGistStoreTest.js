import { spy } from 'sinon';

import { expect } from 'chai';

import { StarredGistStore, __RewireAPI__ as StarredGistStoreRewire } from '../../app/stores/StarredGistStore';
import dispatcher from '../../app/dispatcher';
import { loadStarredGists } from '../../app/actions/GistActions';

describe('stores/StarredGistStore', () => {
    let starredGistStore;

    beforeEach(() => {
        starredGistStore = new StarredGistStore();
        starredGistStore.dispatchToken = dispatcher.register(starredGistStore.handleAction.bind(starredGistStore));
    });

    afterEach(() => {
        dispatcher.unregister(starredGistStore.dispatchToken);
    });

    describe('starting starred gist load action', () => {
        let loadStarredGistsSpy;

        beforeEach(() => {
            loadStarredGistsSpy = spy();
            StarredGistStoreRewire.__Rewire__('loadStarredGists', loadStarredGistsSpy);
        });

        afterEach(() => {
            StarredGistStoreRewire.__ResetDependency__('loadStarredGists');
        });

        it('should fire change event', () => {
            const changeSpy = spy();
            starredGistStore.on('change', changeSpy);
            loadStarredGists();
            expect(changeSpy.calledOnce).to.equal(true);
        });

        it('should fire overtake event', () => {
            const overtakeSpy = spy();
            starredGistStore.on('overtake', overtakeSpy);
            loadStarredGists();
            expect(overtakeSpy.calledOnce).to.equal(true);
        });

        it('should reset error and loader', () => {
            spy(starredGistStore, 'reload');
            loadStarredGists();
            expect(starredGistStore.reload.calledOnce).to.equal(true);
            starredGistStore.reload.restore();
        });

        it('should call api', () => {
            loadStarredGists();
            expect(loadStarredGistsSpy.calledOnce).to.equal(true);
        });
    });
});

import { spy, stub } from 'sinon';
import { expect } from 'chai';
import { LabelStore } from '../../app/stores/LabelStore';
import UserStore from '../../app/stores/UserStore';
import dispatcher from '../../app/dispatcher';
import { setLabel, unsetLabel } from '../../app/actions/LabelActions';
import { deleted } from '../../app/actions/GistActions';
import { logOut } from '../../app/actions/AuthActions';

describe('stores/LabelStore', () => {
    let labelStore;

    beforeEach(() => {
        localStorage.setItem('gist-explorer-ls-labels-randomuser', JSON.stringify({ testLabel: ['1234', '4567']}));
        labelStore = new LabelStore();
        labelStore.dispatchToken = dispatcher.register(labelStore.handleAction.bind(labelStore));
    });

    afterEach(() => {
        localStorage.removeItem('gist-explorer-ls-labels-randomuser');
        dispatcher.unregister(labelStore.dispatchToken);
    });

    it('should have no labels in the begining', () => {
        expect(labelStore.getLabels()).to.deep.equal([]);
    });

    it('should load labels for current user', () => {
        stub(UserStore, 'getUsername').returns('randomuser');
        labelStore.loadLabels();
        expect(labelStore.getLabels()).to.deep.equal(['testLabel']);
        UserStore.getUsername.restore();
    });

    it('should allow setting labels', () => {
        labelStore.setLabel('123', '456');
    });

    it('should allow unsetting labels', () => {
        labelStore.setLabel('123', '456');
        labelStore.unsetLabel('123', '456');
    });

    it('should save labels on set', () => {
        labelStore.setLabel('123', '456');
        expect(labelStore.getLabelsForGist('123')).to.deep.equal(['456']);
    });

    it('should save labels on unset', () => {
        labelStore.setLabel('123', '456');
        labelStore.setLabel('123', '789');
        labelStore.setLabel('123', 'abc');
        labelStore.unsetLabel('123', 'abc');
        expect(labelStore.getLabelsForGist('123')).to.deep.equal(['456', '789']);
    });

    it('should return all labels', () => {
        labelStore.setLabel('123', '456');
        labelStore.setLabel('xyz', '789');
        labelStore.setLabel('4567', 'abc');
        expect(labelStore.getLabels()).to.deep.equal(['456', '789', 'abc']);
    });

    it('should return all labels for a given gist', () => {
        labelStore.setLabel('123', '456');
        labelStore.setLabel('123', '789');
        labelStore.setLabel('123', 'abc');
        expect(labelStore.getLabelsForGist('123')).to.deep.equal(['456', '789', 'abc']);
    });

    it('should verify whether a gist has a given label', () => {
        labelStore.setLabel('123', 'abc');
        expect(labelStore.hasLabels('123', 'abc')).to.equal(true);
    });

    it('should verify whether a gist has given labels (multiple)', () => {
        labelStore.setLabel('789', 'abc');
        labelStore.setLabel('789', 'def');
        expect(labelStore.hasLabels('789', 'def', 'abc')).to.equal(true);
    });

    it('should verify whether a gist has given labels (multiple, negative)', () => {
        labelStore.setLabel('789', 'abc');
        expect(labelStore.hasLabels('789', 'def', 'abc')).to.equal(false);
    });

    it('should be possible to remove gist reference across all labels', () => {
        labelStore.setLabel('789', 'abc');
        labelStore.setLabel('789', 'def');
        labelStore.setLabel('123', 'def');
        labelStore.setLabel('123', 'def');
        labelStore.removeGistFromLabels('789');
        expect(labelStore.getLabels()).to.deep.equal(['def']);
    });

    describe('setLabel action', () => {
        it('should fire a change event', () => {
            const changeSpy = spy();
            labelStore.on('change', changeSpy);
            setLabel('123', '456');
            expect(changeSpy.called).to.equal(true);
        });

        it('should set label', () => {
            spy(labelStore, 'setLabel');
            setLabel('123', '456');
            expect(labelStore.setLabel.calledOnce).to.equal(true);
            labelStore.setLabel.restore();
        });
    });

    describe('unsetLabel action', () => {
        it('should fire a change event', () => {
            const changeSpy = spy();
            labelStore.on('change', changeSpy);
            unsetLabel('123', '456');
            expect(changeSpy.called).to.equal(true);
        });

        it('should unset label', () => {
            spy(labelStore, 'unsetLabel');
            unsetLabel('123', '456');
            expect(labelStore.unsetLabel.calledOnce).to.equal(true);
            labelStore.unsetLabel.restore();
        });
    });

    describe('gist deleted action', () => {
        it('should fire a change event', () => {
            const changeSpy = spy();
            labelStore.on('change', changeSpy);
            deleted('123');
            expect(changeSpy.called).to.equal(true);
        });

        it('should remove the gist reference from labels', () => {
            spy(labelStore, 'removeGistFromLabels');
            deleted('123');
            expect(labelStore.removeGistFromLabels.calledOnce).to.equal(true);
            labelStore.removeGistFromLabels.restore();
        });
    });

    describe('log out action', () => {
        it('should fire a change event', () => {
            const changeSpy = spy();
            labelStore.on('change', changeSpy);
            logOut();
            expect(changeSpy.called).to.equal(true);
        });

        it('should reset labels', () => {
            labelStore.setLabel('987', '321');
            logOut();
            expect(labelStore.getLabels()).to.deep.equal([]);
        });
    });
});
import { spy, stub } from 'sinon';

import { expect } from 'chai';

import { GistStore, __RewireAPI__ as GistStoreRewire } from '../../app/stores/GistStore';
import dispatcher from '../../app/dispatcher';
import LabelStore from '../../app/stores/LabelStore';
import FilterStore from '../../app/stores/FilterStore';
import { loadGists, gistsLoaded, gistsLoadFailed, created, deleteGist, deleted } from '../../app/actions/GistActions';
import { authorized, logOut } from '../../app/actions/AuthActions';
import { setFilter } from '../../app/actions/FilterActions';
import { setLabel } from '../../app/actions/LabelActions';

describe('stores/GistStore', () => {
    let gistStore;

    beforeEach(() => {
        gistStore = new GistStore();
        gistStore.dispatchToken = dispatcher.register(gistStore.handleAction.bind(gistStore));
    });

    afterEach(() => {
        dispatcher.unregister(gistStore.dispatchToken);
    });

    it('should be created with no gists', () => {
        expect(gistStore.getGists()).to.deep.equal([]);
    });

    it('should be possible to set gists', () => {
        gistStore.setGists([
            'gist'
        ]);
    });

    it('should be possible to retrieve gists', () => {
        gistStore.setGists([
            'gist'
        ]);
        expect(gistStore.getGists()).to.deep.equal(['gist']);
    });

    it('should return gists that passed filtering', () => {
        stub(LabelStore, 'hasLabels').returns(false);

        gistStore.setGists([
            {
                id: '123'
            }
        ]);

        expect(gistStore.getGists()).to.deep.equal([]);

        LabelStore.hasLabels.restore();
    });

    describe('gist created action', () => {
        let loadGistsSpy;

        beforeEach(() => {
            loadGistsSpy = spy();
            GistStoreRewire.__Rewire__('loadGists', loadGistsSpy);
        });

        afterEach(() => {
            GistStoreRewire.__ResetDependency__('loadGists');
        });

        it('should fire change event', () => {
            const changeSpy = spy();
            gistStore.on('change', changeSpy);
            created('created-gist');
            expect(changeSpy.calledOnce).to.equal(true);
        });

        it('should fire overtake event', () => {
            const overtakeSpy = spy();
            gistStore.on('overtake', overtakeSpy);
            created('created-gist');
            expect(overtakeSpy.calledOnce).to.equal(true);
        });

        it('should reset error and loader', () => {
            spy(gistStore, 'reload');
            created('created-gist');
            expect(gistStore.reload.calledOnce).to.equal(true);
            gistStore.reload.restore();
        });

        it('should call api', () => {
            created('created-gist');
            expect(loadGistsSpy.calledOnce).to.equal(true);
        });
    });

    describe('gist deleted action', () => {
        let loadGistsSpy;

        beforeEach(() => {
            loadGistsSpy = spy();
            GistStoreRewire.__Rewire__('loadGists', loadGistsSpy);
        });

        afterEach(() => {
            GistStoreRewire.__ResetDependency__('loadGists');
        });

        it('should fire change event', () => {
            const changeSpy = spy();
            gistStore.on('change', changeSpy);
            deleted('123');
            expect(changeSpy.calledOnce).to.equal(true);
        });

        it('should fire overtake event', () => {
            const overtakeSpy = spy();
            gistStore.on('overtake', overtakeSpy);
            deleted('123');
            expect(overtakeSpy.calledOnce).to.equal(true);
        });

        it('should reset error and loader', () => {
            spy(gistStore, 'reload');
            deleted('123');
            expect(gistStore.reload.calledOnce).to.equal(true);
            gistStore.reload.restore();
        });

        it('should call api', () => {
            deleted('123');
            expect(loadGistsSpy.calledOnce).to.equal(true);
        });
    });

    describe('starting gist load action', () => {
        let loadGistsSpy;

        beforeEach(() => {
            loadGistsSpy = spy();
            GistStoreRewire.__Rewire__('loadGists', loadGistsSpy);
        });

        afterEach(() => {
            GistStoreRewire.__ResetDependency__('loadGists');
        });

        it('should fire change event', () => {
            const changeSpy = spy();
            gistStore.on('change', changeSpy);
            loadGists();
            expect(changeSpy.calledOnce).to.equal(true);
        });

        it('should fire overtake event', () => {
            const overtakeSpy = spy();
            gistStore.on('overtake', overtakeSpy);
            loadGists();
            expect(overtakeSpy.calledOnce).to.equal(true);
        });

        it('should reset error and loader', () => {
            spy(gistStore, 'reload');
            loadGists();
            expect(gistStore.reload.calledOnce).to.equal(true);
            gistStore.reload.restore();
        });

        it('should call api', () => {
            loadGists();
            expect(loadGistsSpy.calledOnce).to.equal(true);
        });
    });

    describe('authorization action', () => {
        let loadGistsSpy;

        beforeEach(() => {
            loadGistsSpy = spy();
            GistStoreRewire.__Rewire__('loadGists', loadGistsSpy);
        });

        afterEach(() => {
            GistStoreRewire.__ResetDependency__('loadGists');
        });

        it('should fire change event', () => {
            const changeSpy = spy();
            gistStore.on('change', changeSpy);
            authorized('123');
            expect(changeSpy.calledOnce).to.equal(true);
        });

        it('should fire overtake event', () => {
            const overtakeSpy = spy();
            gistStore.on('overtake', overtakeSpy);
            authorized('123');
            expect(overtakeSpy.calledOnce).to.equal(true);
        });

        it('should reset error and loader', () => {
            spy(gistStore, 'reload');
            authorized('123');
            expect(gistStore.reload.calledOnce).to.equal(true);
            gistStore.reload.restore();
        });

        it('should call api', () => {
            authorized('123');
            expect(loadGistsSpy.calledOnce).to.equal(true);
        });
    });

    describe('logging out action', () => {
        it('should fire change event', () => {
            const changeSpy = spy();
            gistStore.on('change', changeSpy);
            logOut();
            expect(changeSpy.calledOnce).to.equal(true);
        });

        it('should empty the gists list', () => {
            gistStore.setGists([1,2,3]);
            logOut();
            expect(gistStore.getGists()).to.deep.equal([]);
        });
    });

    describe('setting new label action', () => {
        it('should fire change event', () => {
            const changeSpy = spy();
            gistStore.on('change', changeSpy);
            setLabel('123', '123');
            expect(changeSpy.calledOnce).to.equal(true);
        });

        it('should refresh the gists list', () => {
            spy(gistStore, 'setGists');
            setLabel('123', '123');
            expect(gistStore.setGists.calledOnce).to.equal(true);
            gistStore.setGists.restore();
        });

        it('should update the filter', () => {
            gistStore.setGists([
                {
                    id: '123'
                }
            ]);

            stub(gistStore, 'filter').returns([]);
            setLabel('123', '123');
            expect(gistStore.getGists().length).to.equal(0);
            gistStore.filter.restore();
        });
    });

    describe('adjusting filter action', () => {
        before(() => {
            stub(FilterStore, 'setFilter');
        });

        after(() => {
            FilterStore.setFilter.restore();
        });

        it('should fire change event', () => {
            const changeSpy = spy();
            gistStore.on('change', changeSpy);
            setFilter('123');
            expect(changeSpy.calledOnce).to.equal(true);
        });

        it('should refresh the gists list', () => {
            spy(gistStore, 'setGists');
            setFilter('123');
            expect(gistStore.setGists.calledOnce).to.equal(true);
            gistStore.setGists.restore();
        });

        it('should update the filter', () => {
            gistStore.setGists([
                {
                    id: '123'
                }
            ]);

            stub(gistStore, 'filter').returns([]);
            setFilter('123');
            expect(gistStore.getGists().length).to.equal(0);
            gistStore.filter.restore();
        });
    });

    describe('finishing gists load action', () => {
        it('should fire change event', () => {
            const changeSpy = spy();
            loadGists();
            gistStore.on('change', changeSpy);
            gistsLoaded([1,2,3]);
            expect(changeSpy.calledOnce).to.equal(true);
        });

        it('should not fire change event when load was not started', () => {
            const changeSpy = spy();
            gistStore.on('change', changeSpy);
            gistsLoaded([1,2,3]);
            expect(changeSpy.called).to.equal(false);
        });

        it('should turn off loader', () => {
            gistStore.setLoading();
            gistsLoaded([1,2,3]);
            expect(gistStore.isLoading()).to.equal(false);
        });
    });

    describe('failing gists load action', () => {
        it('should fire change event', () => {
            const changeSpy = spy();
            loadGists();
            gistStore.on('change', changeSpy);
            gistsLoadFailed('bla');
            expect(changeSpy.calledOnce).to.equal(true);
        });

        it('should not fire change event when load was not started', () => {
            const changeSpy = spy();
            gistStore.on('change', changeSpy);
            gistsLoadFailed('bla');
            expect(changeSpy.called).to.equal(false);
        });

        it('should turn off loader', () => {
            gistStore.setLoading();
            gistsLoadFailed('bla');
            expect(gistStore.isLoading()).to.equal(false);
        });
    });

    describe('delete gist action', () => {
        let deleteGistSpy;

        beforeEach(() => {
            deleteGistSpy = spy();
            GistStoreRewire.__Rewire__('deleteGist', deleteGistSpy);
        });

        afterEach(() => {
            GistStoreRewire.__ResetDependency__('deleteGist');
        });

        it('should fire change event', () => {
            const changeSpy = spy();
            gistStore.on('change', changeSpy);
            deleteGist('123');
            expect(changeSpy.calledOnce).to.equal(true);
        });

        it('should reset error and loader', () => {
            spy(gistStore, 'reload');
            deleteGist('123');
            expect(gistStore.reload.calledOnce).to.equal(true);
            gistStore.reload.restore();
        });

        it('should call api', () => {
            deleteGist('123');
            expect(deleteGistSpy.calledOnce).to.equal(true);
        });
    });
});

import { stub } from 'sinon';
import dispatcher from '../app/dispatcher';

import GistStore from '../app/stores/GistStore';
import StarredGistStore from '../app/stores/StarredGistStore';
import CurrentGistStore from '../app/stores/CurrentGistStore';
import AuthStore from '../app/stores/AuthStore';
import FilterStore from '../app/stores/FilterStore';
import LabelStore from '../app/stores/LabelStore';
import UserStore from '../app/stores/UserStore';

import './api/AuthTest';
import './api/GistsTest';
import './api/UserTest';
import './stores/AsyncStoreTest';
import './stores/AuthStoreTest';
import './stores/CurrentGistStoreTest';
import './stores/FilterStoreTest';
import './stores/GistStoreTest';
import './stores/LabelStoreTest';
import './stores/StarredGistStoreTest';
import './stores/UserStoreTest';

// unregister default stores
dispatcher.unregister(GistStore.dispatchToken);
dispatcher.unregister(StarredGistStore.dispatchToken);
dispatcher.unregister(CurrentGistStore.dispatchToken);
dispatcher.unregister(AuthStore.dispatchToken);
dispatcher.unregister(FilterStore.dispatchToken);
dispatcher.unregister(LabelStore.dispatchToken);
dispatcher.unregister(UserStore.dispatchToken);

// remove dispatcher link between stores
stub(dispatcher, 'waitFor');
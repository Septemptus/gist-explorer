const express =  require('express');
const compression = require('compression');
const axios = require('axios');

var CLIENT_ID = 'c0d50e307111455f6ce7';
const SECRET = 'e2aa39b42a3007dbe78d922e30cb9ba21cc70074';
const app = express();

app.use(express.static(__dirname + '/build'));
app.use(compression());

app.get('/login/:code', (req, res) => {
    axios.post('https://github.com/login/oauth/access_token',
        {
            client_id: CLIENT_ID,
            client_secret: SECRET,
            code: req.params.code
        })
        .then((response) => {
            res.end(response.data.match(/access_token=([^&]+)/)[1]);
        })
        .catch(() => {
            res.statusCode = 500;
            res.end();
        });
});

app.listen(process.env.PORT || 665);